/*
	DIB.H
	-----
*/
#ifndef DIB_H_
#define DIB_H_

#ifdef _WIN32
	#include <windows.h>
#else
	#include <stdint.h>

	typedef uint8_t  CHAR;
	typedef uint16_t WORD;
	typedef uint32_t DWORD;

	typedef int8_t  BYTE;
	typedef int16_t SHORT;
	typedef int32_t LONG;

	typedef LONG INT;
	typedef INT BOOL;

	typedef struct tagBITMAPINFOHEADER
		{
		DWORD biSize;
		LONG  biWidth;
		LONG  biHeight;
		WORD  biPlanes;
		WORD  biBitCount;
		DWORD biCompression;
		DWORD biSizeImage;
		LONG  biXPelsPerMeter;
		LONG  biYPelsPerMeter;
		DWORD biClrUsed;
		DWORD biClrImportant;
		} BITMAPINFOHEADER, *PBITMAPINFOHEADER;

	typedef struct tagRGBQUAD
		{
		BYTE rgbBlue;
		BYTE rgbGreen;
		BYTE rgbRed;
		BYTE rgbReserved;
		} RGBQUAD;

#endif

#define TEXT_COLOUR 7
#define BACKGROUND_COLOUR 0
#define PALLETTE_BACKGROUND_BASE 8
#define PALLETTE_GRAPHICS_BASE 16
#define PALLETTE_MIXED_GRAPHICS_BASE 20
#define PALLETTE_COMBINED_GRAPHICS_BASE 36
#define PRINTER_TEXT_COLOUR 56
#define PRINTER_BOLD_TEXT_COLOUR 57
#define PRINTER_PERFORATION_COLOUR 58

/*
	BITMAPSINFO256
	--------------
*/
typedef struct
{
BITMAPINFOHEADER bmiHeader;
RGBQUAD bmiColors[256];		// fake a 256 colour bitmap (we only use  40 of them, but want a byte-per-pixel frame buffer)
} BITMAPINFO256;

/*
	PALLETTE
	--------
*/
static RGBQUAD pallette[] = 
{
{0x00, 0x00, 0x00},		// black
{0x00, 0x00, 0xFF},		// red
{0x00, 0xFF, 0x00},		// green
{0x00, 0xFF, 0xFF},		// yellow
{0xFF, 0x00, 0x00},		// blue
{0xFF, 0x00, 0xFF},		// magenta
{0xFF, 0xFF, 0x00},		// cyan
{0xFF, 0xFF, 0xFF},		// white

{0x00, 0x00, 0x00},		// Background black
{0x00, 0x00, 0x80},		// Background red
{0x00, 0x80, 0x00},		// Background green
{0x00, 0x80, 0x80},		// Background yellow
{0x80, 0x00, 0x00},		// Background blue
{0x80, 0x00, 0x80},		// Background magenta
{0x80, 0x80, 0x00},		// Background cyan
{0x80, 0x80, 0x80},		// Background white

{0xFF, 0x00, 0x00},		// graphics blue
{0x00, 0xFF, 0x00},		// graphics green
{0x00, 0x00, 0xFF},		// graphics red
{0xFF, 0xFF, 0xFF},		// graphics white

{0xFF, 0x00, 0x00},		// graphics 00 00
{0xFF, 0xFF, 0x00},		// graphics 00 01
{0xFF, 0x00, 0xFF},		// graphics 00 10
{0xFF, 0xFF, 0xFF},		// graphics 00 11
{0xFF, 0xFF, 0x00},		// graphics 01 00
{0x00, 0xFF, 0x00},		// graphics 01 01
{0x00, 0xFF, 0xFF},		// graphics 01 10
{0xFF, 0xFF, 0xFF},		// graphics 01 11
{0xFF, 0x00, 0xFF},		// graphics 10 00
{0x00, 0xFF, 0xFF},		// graphics 10 01
{0x00, 0x00, 0xFF},		// graphics 10 10
{0xFF, 0xFF, 0xFF},		// graphics 10 11

{0xFF, 0x00, 0x00},		// graphics blue
{0x00, 0xFF, 0x00},		// graphics green
{0x00, 0x00, 0xFF},		// graphics red
{0xFF, 0xFF, 0xFF},		// graphics white

{0xFF, 0x00, 0x00},		// graphics 00 00
{0xFF, 0xFF, 0x00},		// graphics 00 01
{0xFF, 0x00, 0xFF},		// graphics 00 10
{0xFF, 0xFF, 0xFF},		// graphics 00 11
{0xFF, 0xFF, 0x00},		// graphics 01 00
{0x00, 0xFF, 0x00},		// graphics 01 01
{0x00, 0xFF, 0xFF},		// graphics 01 10
{0xFF, 0xFF, 0xFF},		// graphics 01 11
{0xFF, 0x00, 0xFF},		// graphics 10 00
{0x00, 0xFF, 0xFF},		// graphics 10 01
{0x00, 0x00, 0xFF},		// graphics 10 10
{0xFF, 0xFF, 0xFF},		// graphics 10 11

{0xFF, 0xFF, 0xFF},		// graphics 11 00
{0xFF, 0xFF, 0xFF},		// graphics 11 01
{0xFF, 0xFF, 0xFF},		// graphics 11 10
{0xFF, 0xFF, 0xFF}, 		// graphics 11 11

{0xFF, 0x00, 0x00},		// graphics blue
{0x00, 0xFF, 0x00},		// graphics green
{0x00, 0x00, 0xFF},		// graphics red
{0xFF, 0xFF, 0xFF},		// graphics white

{0xD0, 0xD0, 0xD0},		// printer ink
{0xFF, 0xFF, 0xFF},		// printer bold (emphasized) ink
{0x80, 0x80, 0x80}			// printer paper perforation colour
};

#endif /* DIB_H_ */
