/*
	POLY_ROMS.H
	-----------
*/
#ifndef POLY_ROMS_H_
#define POLY_ROMS_H_

extern unsigned char poly_basic_34_1[];
extern unsigned char poly_basic_34_2[];
extern unsigned char poly_basic_34_3[];
extern unsigned char poly_basic_34_4[];
extern unsigned char poly_bios_34[];

extern unsigned char poly_basic_31_1[];
extern unsigned char poly_basic_31_2[];
extern unsigned char poly_basic_31_3[];
extern unsigned char poly_basic_31_4[];
extern unsigned char poly_bios_31[];

extern unsigned char poly_basic_30_1[];
extern unsigned char poly_basic_30_2[];
extern unsigned char poly_basic_30_3[];
extern unsigned char poly_basic_30_4[];
extern unsigned char poly_bios_30[];
								
extern unsigned char poly_basic_23_1[];
extern unsigned char poly_basic_23_2[];
extern unsigned char poly_basic_23_3[];
extern unsigned char poly_basic_23_4[];
extern unsigned char poly_bios_23[];

#endif /* POLY_ROMS_H_ */
