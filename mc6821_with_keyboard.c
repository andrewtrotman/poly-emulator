/*
	MC6821_WITH_KEYBOARD.C
	----------------------
*/

#include "mc6821_with_keyboard.h"

/*
	MC6821_WITH_KEYBOARD::MC6821_WITH_KEYBOARD()
	--------------------------------------------
*/
mc6821_with_keyboard::mc6821_with_keyboard(poly *client)
{
this->client = client;
}

/*
	MC6821_WITH_KEYBOARD::IRQB1()
	-----------------------------
*/
void mc6821_with_keyboard::irqb1(void)
{
mc6821::irqb1();
client->keyboard_irq();
}

/*
	MC6821_WITH_KEYBOARD::D_IRQB()
	------------------------------
*/
void mc6821_with_keyboard::d_irqb(void)
{
mc6821::d_irqb();
client->keyboard_d_irq();
}






