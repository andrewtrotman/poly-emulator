/*
	MC6850_WITH_MX_80.H
	-------------------
*/
#ifndef MC6850_WITH_MX_80_H_
#define MC6850_WITH_MX_80_H_

#ifdef _WIN32
	#include <windows.h>
#endif

#include "mc6850.h"
#include "mx_80.h"

/*
	class MC6850_WITH_MX_80
	-----------------------
*/
class mc6850_with_mx_80 : public mc6850, public mx_80
{
protected:
	virtual byte poll(void) { return 0; }
	virtual byte in(void) { return 0; }
	virtual void out(byte value) { putch(value ); }

public:
	#ifdef _WIN32
		mc6850_with_mx_80(HINSTANCE hInstance, const char *title) : mc6850(), mx_80(hInstance) { create_window(title); }
	#else
		mc6850_with_mx_80(long hInstance, const char *title) : mc6850(), mx_80(hInstance) { }
	#endif
	~mc6850_with_mx_80() {}
} ;


#endif /* MC6850_WITH_MX_80_H_ */
