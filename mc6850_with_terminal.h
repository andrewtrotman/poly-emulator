/*
	MC6850_WITH_TERMINAL.H
	----------------------
*/
#ifndef MC6850_WITH_TERMINAL_H_
#define MC6850_WITH_TERMINAL_H_

#ifdef _WIN32
	#include <windows.h>
#endif
#include "mc6850.h"
#include "terminal.h"

/*
	class MC6850_WITH_TERMINAL
	--------------------------
*/
class mc6850_with_terminal : public mc6850, public terminal
{
protected:
	virtual byte poll(void) { return kbhit(); } 
	virtual byte in(void) { return getch(); }
	virtual void out(byte value) { putch(value ); }

public:
#ifdef _WIN32
		mc6850_with_terminal(HINSTANCE hInstance, const char *title, long mode = 0) : mc6850(), terminal(hInstance) { create_window(title, mode); }
#else
		mc6850_with_terminal(long hInstance, const char *title, long mode = 0) : mc6850(), terminal(hInstance) { create_window(title, mode); }
#endif
	~mc6850_with_terminal() {}
} ;

#endif /* MC6850_WITH_TERMINAL_H_ */
