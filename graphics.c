/*
	GRAPHICS.C
	----------
*/
#include "graphics.h"
#include "dib.h"

/*
	GRAPHICS::RENDER_PAGE()
	-----------------------
*/
void graphics::render_page(unsigned char *canvas, unsigned char *memory, unsigned char mix_colour)
{
long x, y;
unsigned char *byte, colour;
unsigned char *pixel_base;

byte = memory;

for (y = 0; y < 204; y++)
	for (x = 0; x < 40; x++)
		{
		if ((*byte & 0x3F) != 0)
			{
			pixel_base = canvas + y * 480 + x * 12;
			colour = (*byte >> 6) + (mix_colour << 2) + PALLETTE_MIXED_GRAPHICS_BASE;
			if (*byte & 0x20)
				{
				pixel_base[0] = colour;
				pixel_base[1] = colour;
				}
			if (*byte & 0x10)
				{
				pixel_base[2] = colour;
				pixel_base[3] = colour;
				}
			if (*byte & 0x08)
				{
				pixel_base[4] = colour;
				pixel_base[5] = colour;
				}
			if (*byte & 0x04)
				{
				pixel_base[6] = colour;
				pixel_base[7] = colour;
				}
			if (*byte & 0x02)
				{
				pixel_base[8] = colour;
				pixel_base[9] = colour;
				}
			if (*byte & 0x01)
				{
				pixel_base[10] = colour;
				pixel_base[11] = colour;
				}
			}
		byte++;
		}
}

/*
	GRAPHICS::RENDER_MIXED_PAGES()
	------------------------------
*/
void graphics::render_mixed_pages(unsigned char *canvas, unsigned char *screen2, unsigned char *screen4)
{
long x, y;
unsigned char *byte1, *byte2, byte, colour;
unsigned char *pixel_base;

byte1 = screen2;
byte2 = screen4;

for (y = 0; y < 204; y++)
	for (x = 0; x < 40; x++)
		{
		byte = *byte1 & *byte2;
		if ((byte & 0x3F) != 0)
			{
			pixel_base = canvas + y * 480 + x * 12;
			colour = ((*byte1 >> 6) | ((*byte2 & 0xC0) >> 4)) + PALLETTE_MIXED_GRAPHICS_BASE;

			if (byte & 0x20)
				{
				pixel_base[0] = colour;
				pixel_base[1] = colour;
				}
			if (byte & 0x10)
				{
				pixel_base[2] = colour;
				pixel_base[3] = colour;
				}
			if (byte & 0x08)
				{
				pixel_base[4] = colour;
				pixel_base[5] = colour;
				}
			if (byte & 0x04)
				{
				pixel_base[6] = colour;
				pixel_base[7] = colour;
				}
			if (byte & 0x02)
				{
				pixel_base[8] = colour;
				pixel_base[9] = colour;
				}
			if (byte & 0x01)
				{
				pixel_base[10] = colour;
				pixel_base[11] = colour;
				}
			}
		byte1++;
		byte2++;
		}
}


/*
	ASPT_SCREEN::PAINT_COMBINED_GRAPHICS_PAGE()
	-------------------------------------------
*/
void graphics::render_combined_pages(unsigned char *canvas, unsigned char *screen2, unsigned char *screen4)
{
long x, y;
unsigned char *byte1, *byte2, colour;
unsigned char *pixel_base;

byte1 = screen2;
byte2 = screen4;

for (y = 0; y < 204; y++)
	for (x = 0; x < 40; x++)
		{
		if ((*byte1 & 0x3F) != 0)
			{
			pixel_base = canvas + y * 480 + x * 12;
			colour = (*byte1 >> 6) + PALLETTE_GRAPHICS_BASE;
			if (*byte1 & 0x20)
				pixel_base[0] = colour;
			if (*byte1 & 0x10)
				pixel_base[1] = colour;
			if (*byte1 & 0x08)
				pixel_base[2] = colour;
			if (*byte1 & 0x04)
				pixel_base[3] = colour;
			if (*byte1 & 0x02)
				pixel_base[4] = colour;
			if (*byte1 & 0x01)
				pixel_base[5] = colour;
			}
		if ((*byte2 & 0x3F) != 0)
			{
			pixel_base = canvas + y * 480 + x * 12;
			colour = (*byte2 >> 6) + PALLETTE_GRAPHICS_BASE;
			if (*byte2 & 0x20)
				pixel_base[6] = colour;
			if (*byte2 & 0x10)
				pixel_base[7] = colour;
			if (*byte2 & 0x08)
				pixel_base[8] = colour;
			if (*byte2 & 0x04)
				pixel_base[9] = colour;
			if (*byte2 & 0x02)
				pixel_base[10] = colour;
			if (*byte2 & 0x01)
				pixel_base[11] = colour;
			}
		byte1++;
		byte2++;
		}
}

