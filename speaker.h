/*
	SPEAKER.H
	---------
*/
#ifndef SPEAKER_H_
#define SPEAKER_H_

#ifdef _WIN32
	#include <windows.h>
#endif

/*
	class SPEAKER
	-------------
*/
class speaker
{
private:
	long frequency;
	double angle;
	long buffer_size;
	unsigned char *buffer;
#ifdef _WIN32
	HINSTANCE hInstance;
	HWAVEOUT hWaveOut;
	WAVEHDR header;
#else
	long hInstance;
	long hWaveOut;
	long header;
#endif

protected:
	void fill_buffer(unsigned char *buffer);

public:
	speaker();
	virtual ~speaker();
	void play(long frequency);
	void quiet(void);
	void reset(void) { quiet(); }
} ;

#endif /* SPEAKER_H_ */

