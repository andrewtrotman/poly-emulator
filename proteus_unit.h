/*
	PROTEUS_UNIT.H
	--------------
*/
#ifndef PROTEUS_UNIT_H_
#define PROTEUS_UNIT_H_

#ifdef _WIN32
	#include <windows.h>
#endif
#include "proteus.h"

/*
	class PROTEUS_UNIT
	------------------
*/
class proteus_unit
{
public:
	enum { ACTIVE, PASSIVE };

private:
	proteus *server;
	long mode;
	#ifdef _WIN32
		HINSTANCE hInstance;
		HBITMAP bmp_about;
		HBITMAP bmp_proteus;
		HWND window;
	#else
		long hInstance;
		long bmp_about;
		long bmp_proteus;
		long window;
	#endif

	long bmp_about_width, bmp_about_height;
	long bmp_proteus_width, bmp_proteus_height;

public:
	#ifdef _WIN32
		proteus_unit(HINSTANCE hInstance);
	#else
		proteus_unit(long hInstance);
	#endif
	virtual ~proteus_unit();

	#ifdef _WIN32
		static void load_disk(proteus *server, HWND hwnd, long menu);
	#else
		static void load_disk(proteus *server, long hwnd, long menu);
	#endif

	unsigned char *load_rom(long menu);

	#ifdef _WIN32
		INT_PTR CALLBACK about_box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
		LRESULT windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	#else
		bool about_box(long hDlg, long message, long wParam, long lParam);
		long windows_callback(long hwnd, long message, long wParam, long lParam);
	#endif
	
	proteus *create_window(const char *window_title, long mode = ACTIVE);
} ;

#endif /* PROTEUS_UNIT_H_ */
