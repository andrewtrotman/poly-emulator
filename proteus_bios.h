/*
	PROTEUS_BIOS.H
	--------------
*/
#ifndef PROTEUS_BIOS_H_
#define PROTEUS_BIOS_H_

extern unsigned char proteus_bios_30[0x1000];
extern unsigned char proteus_bios_31[0x1000];

#endif /* PROTEUS_BIOS_H_ */
