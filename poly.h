/*
	POLY.H
	------
*/
#ifndef POLY_H_
#define POLY_H_

#ifdef _WIN32
	#include <windows.h>
#endif
#include "proteus.h"
#include "graphics.h"
#include "saa5050.h"

/*
	class POLY
	----------
*/
class poly : public proteus
{
friend class poly_unit;

private:
	static const int KEYBOARD_BUFFER_LENGTH = 128;		// length of the (Windows) keyboard typeahead buffer

public:
	enum { UPDATE_DISPLAY_1 = 1, UPDATE_DISPLAY_2, UPDATE_DISPLAY_3, UPDATE_DISPLAY_4, UPDATE_DISPLAY_5, UPDATE_DISPLAY_FLAGS };

private:
	class key_message
	{
	public:
		unsigned char ascii, cb1, cb2;
	} ;

private:
	#ifdef _WIN32
		HINSTANCE hInstance;
	#endif
	long prot;								// are we in protected mode?
	long long cycles_before_leaving_prot;	// when leaving prot it takes 2 cycles to switch and this is that count
	unsigned char memory[0x20000];			// the Poly has Dynamic Address Translation mapping to 128KB 
	unsigned char dat[16];					// the Poly Dynamic Address Translation (DAT) table
	long dat_bank;							// the Poly DAT has 2 banks (called Memory Map 1 and Memory Map 2)
	unsigned char video_memory[0x1000];		// Video RAM (used by CPU and SAA5050)
	unsigned char bios[0x1000];				// Poly BIOS
	unsigned char baud_rate_register;		// exactly that!

	long timer_irqpend;						// the timer has generated an interrupt
	long keyboard_irqpend;					// the keyboard has generated an interrupt
	long network_irqpend;					// the network has generated an interrupt

	long rom_version;						// which ROM version to load on reset

public:
	saa5050 *screen_1;						// Poly display 1 (primary TEXT)
	saa5050 *screen_3;						// Poly display 3 (secondary TEXT)
	graphics *graphics_screen;				// Poly display 2, 4, and 5 (GRAPHICS)
	mc6821 *pia2;							// Poly keyboard interface
	long changes;							// what has happened on the Poly  (updates to screen, etc)

private:
	key_message keyboard_buffer[KEYBOARD_BUFFER_LENGTH];
	key_message *keyboard_buffer_read_pos, *keyboard_buffer_write_pos;

protected:
	unsigned long physical(unsigned short address);
	void get_rom(void);

public:
	#ifdef _WIN32
		poly(HINSTANCE hInstance);
	#else
		poly(long hInstance);
	#endif
	~poly();

	virtual byte read(word address);
	virtual void write(word address, byte val);
	virtual void swi(void);
	virtual void swi2(void);
	virtual void swi3(void);
	virtual void do_nmi(void);
	virtual void do_firq(void);
	virtual void do_irq(void);
	virtual void reset(void);

	virtual void step(void);
	long set_rom_version(long version);

	void process_next_keyboard_event(void);
	void putch(unsigned char ascii, unsigned char down = 1);

	/*
		Here we can see the various sources of IRQs
	*/
	virtual void timer_irq(void) 		{ timer_irqpend = 1; }
	virtual void timer_d_irq(void) 		{ timer_irqpend = 0; }
	virtual void keyboard_irq(void) 	{ keyboard_irqpend = 1; }
	virtual void keyboard_d_irq(void) 	{ keyboard_irqpend = 0; }
	virtual void network_irq(void);
	virtual void network_d_irq(void)	{ network_irqpend = 0; }
};

#endif /* POLY_H_ */



