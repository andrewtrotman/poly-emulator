/*
	MAIN.C
	------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include "proteus_unit.h"

/*
	WINMAIN()
	---------
*/
#ifdef _WIN32
	int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	{
	MSG msg;
	long done;
	proteus_unit *server;

	server = new proteus_unit(hInstance);
	server->create_window("Proteus Computer");

	done = false;
	while(!done)
		{
		PeekMessage(&msg, 0, NULL, NULL, PM_NOREMOVE);		// peek but don't remove the message from the queue (that happens below)
		if (msg.message == WM_QUIT)
			done = true;
		else
			{
			GetMessage(&msg, NULL, 0, 0);
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}
		}

	return msg.wParam;
	}
#else
	int main(void)
	{
	long done;
	proteus_unit *server;

	server = new proteus_unit(0);
	server->create_window("Proteus Computer");

	return 0;
	}
		
#endif
