/*
	TERMINAL.H
	----------
*/
#ifndef TERMINAL_H_
#define TERMINAL_H_

#ifdef _WIN32
	#include <windows.h>
#endif

/*
	class TERMINAL
	--------------
*/
class terminal
{
#ifdef _WIN32
	friend static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

private:
	static const int KEYBOARD_BUFFER_LENGTH = 1024;		// length of the keyboard typeahead buffer
	static const int WIDTH = 80;						// screen width in characters
	static const int HEIGHT = 24;						// screen height in characters
	static const int FLASH_INTERVAL = 500;				// 500 ms between cursor flashes
	static const int ESC = 27;							// ASCII for ESC key
	static const int WIDTH_IN_PIXELS = WIDTH * 12;		// window size in pixels
	static const int HEIGHT_IN_PIXELS = HEIGHT * 10;	// window height in pixels
	static const int TABSTOPS = 8;						// tab every 8 characters

public:
	enum { NO_CURSOR = 1, CREATE_ON_WRITE = 2, AUTO_NL = 4 };		// flags for window creation

private:
	#ifdef _WIN32
		HINSTANCE hInstance;
		HWND window;
		HDC bitmap;
	#else
		long hInstance;
		long window;
		long bitmap;
	#endif
	unsigned char *canvas;
	unsigned char *font;
	long flash_state;

	unsigned char keyboard_buffer[KEYBOARD_BUFFER_LENGTH];
	unsigned char *keyboard_buffer_read_pos, *keyboard_buffer_write_pos;

	char screen_buffer[WIDTH * HEIGHT];
	long cursor_x, cursor_y;
	unsigned long last_4_keys;
	long inverse_video;
	long mode, hidden, auto_nl;

protected:
	#ifdef _WIN32
		virtual LRESULT windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	#else
		virtual long windows_callback(long hwnd, long message, long wParam, long lParam);
	#endif
	void print_char(long pos_x, long pos_y, unsigned char ch);
	void paint_text(void);
	#ifdef _WIN32
		void window_paint(HWND window);
	#else
		void window_paint(long window);
	#endif

public:
	#ifdef _WIN32
		terminal(HINSTANCE hInstance);
	#else
		terminal(long hInstance);
	#endif
	virtual ~terminal() {};

	long create_window(const char *window_title, long mode = 0);

	int kbhit(void) { return (keyboard_buffer_read_pos == keyboard_buffer_write_pos) ? 0 : *keyboard_buffer_read_pos; }
	int getch(void);
	void putch(unsigned char character);
} ;

#endif /* TERMINAL_H_ */
