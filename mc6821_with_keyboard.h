/*
	MC6821_WITH_KEYBOARD.H
	----------------------
*/
#ifndef MC6821_WITH_KEYBOARD_H_
#define MC6821_WITH_KEYBOARD_H_

#include "poly.h"
#include "mc6821.h"

/*
	class MC6821_WITH_KEYBOARD
	--------------------------
*/
class mc6821_with_keyboard : public mc6821
{
private:
	poly *client;

protected:
	virtual void irqb1(void);
	virtual void d_irqb(void);

public:
	mc6821_with_keyboard(poly *client);
	virtual ~mc6821_with_keyboard() {}
} ;

#endif /* MC6821_WITH_KEYBOARD_H_ */
