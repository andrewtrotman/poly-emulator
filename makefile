OBJS =usim.obj 							\
		mc6809.obj 						\
		mc6850.obj 						\
		mc6821.obj 						\
		mc6821_with_keyboard.obj 		\
		mc6840.obj						\
		mc6840_with_speaker.obj			\
		mc6854.obj						\
		mc6854_channel.obj				\
		wd1771.obj						\
		saa5050.obj						\
		saa5050_font.obj				\
		graphics.obj					\
		speaker.obj						\
		proteus_bios_30.obj 			\
		proteus_bios_31.obj 			\
		proteus.obj						\
		proteus_unit.obj				\
		poly.obj						\
		poly_unit.obj					\
		terminal.obj					\
		poly_basic_23_1.obj				\
		poly_basic_23_2.obj				\
		poly_basic_23_3.obj				\
		poly_basic_23_4.obj				\
		poly_bios_23.obj				\
		poly_basic_30_1.obj				\
		poly_basic_30_2.obj				\
		poly_basic_30_3.obj				\
		poly_basic_30_4.obj				\
		poly_bios_30.obj				\
		poly_basic_31_1.obj				\
		poly_basic_31_2.obj				\
		poly_basic_31_3.obj				\
		poly_basic_31_4.obj				\
		poly_bios_31.obj				\
		poly_basic_34_1.obj				\
		poly_basic_34_2.obj				\
		poly_basic_34_3.obj				\
		poly_basic_34_4.obj				\
		poly_bios_34.obj				\
		mx_80.obj						\
		mx_80_font.obj					\
		vice_z80/z80.obj				\
		vice_z80/daa.obj				\
		resources/resources.res	

#CFLAGS = -Zi -W3 -D_CRT_SECURE_NO_WARNINGS -nologo 
CFLAGS = -O2 -W3 -D_CRT_SECURE_NO_WARNINGS -nologo

.c.obj:
	@$(CC) -c $(CFLAGS) -Tp $<

all : proteus.exe poly.exe network_poly.exe

proteus.exe : $(OBJS) main_for_proteus.obj
	@$(CC) $(CFLAGS) $(OBJS) main_for_proteus.obj /Feproteus.exe user32.lib gdi32.lib winmm.lib comdlg32.lib /link /fixed:no

poly.exe : $(OBJS) main_for_poly.obj
	@$(CC) $(CFLAGS)  $(OBJS) main_for_poly.obj /Fepoly.exe user32.lib gdi32.lib winmm.lib comdlg32.lib /link /fixed:no

network_poly.exe : $(OBJS) main_for_network_poly.obj
	@$(CC) $(CFLAGS)  $(OBJS) main_for_network_poly.obj /Fenetwork_poly.exe user32.lib gdi32.lib winmm.lib comdlg32.lib /link /fixed:no

vice_z80/z80.obj : vice_z80/z80.cpp vice_z80/daa.cpp
	@cd vice_z80
	@make -nologo
	@cd ..

clean :
	del *.obj *.pdb *.exe *.res *.ilk *.bak /s


