/*
	MC6821.H
	--------
*/
#ifndef MC6821_H_
#define MC6821_H_

#include "typedefs.h"

/*
	class MC6821
	------------
	Motorola 6821 PIA (Parallel Port)
	Default behaviour is to have a single byte buffer attached to the port that can be written to and read from
*/
class mc6821
{
protected:
	byte cra, dda, byte_a;
	byte crb, ddb, byte_b;

protected:
	virtual void irqa1(void)		{ cra |= 0x80; }
	virtual void irqa2(void)		{ cra |= 0x40; }
	virtual void d_irqa(void)		{ cra &= 0x3F; }

	virtual void irqb1(void)		{ crb |= 0x80; }
	virtual void irqb2(void)		{ crb |= 0x40; }
	virtual void d_irqb(void)		{ crb &= 0x3F; }

public:
	mc6821();
	virtual ~mc6821() {}

	byte read(word address);
	void write(word address, byte value);

	virtual byte in_a(void) 		{ return byte_a; }
	virtual void out_a(byte value)	{ byte_a = value; }
	virtual byte in_b(void)			{ return byte_b; }
	virtual void out_b(byte value)	{ byte_b = value; }
	virtual void arrived_a(byte value, byte ca1, byte ca2);
	virtual void arrived_b(byte value, byte cb1, byte cb2);
} ;

#endif /* MC6821_H_ */
