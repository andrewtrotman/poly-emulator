/*
	MC6840_WITH_SPEAKER.C
	---------------------
*/

#include "mc6840_with_speaker.h"


/*
	MC6840_WITH_SPEAKER::MC6840_WITH_SPEAKER()
	------------------------------------------
*/
mc6840_with_speaker::mc6840_with_speaker(poly *cpu) : mc6840_with_irq(cpu)
{
player = new speaker;
}

/*
	MC6840_WITH_SPEAKER::~MC6840_WITH_SPEAKER()
	-------------------------------------------
*/
mc6840_with_speaker::~mc6840_with_speaker()
{
delete player;
}

/*
	MC6840_WITH_SPEAKER::RESET()
	----------------------------
*/
void mc6840_with_speaker::reset(void)
{
player->reset();
mc6840_with_irq::reset();
}

/*
	MC6840_WITH_SPEAKER::WRITE()
	----------------------------
	Poly "pitch" is computed as (502400 / frequency) - 1
*/
void mc6840_with_speaker::write(word address, byte value)
{
long old_rate = timer_3_rate;

mc6840::write(address, value);

if (timer_3_control & 0x80)
	{
	player->quiet();
	player->play(502400 / (timer_3_rate + 1));
	}
else
	player->quiet();
}
