/*
	MX_80.H
	-------
*/
#ifndef MX_80_H_
#define MX_80_H_

#ifdef _WIN32
	#include <windows.h>
#endif

/*
	class MX_80
	-----------
*/
class mx_80
{
#ifdef _WIN32
	friend static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

private:
	static const int WIDTH = 80;						// screen width in characters
	static const int HEIGHT = 24;						// screen height in characters
	static const int ESC = 27;							// ASCII for ESC key
	static const int CHAR_WIDTH = 12;					// width of a character in pixels
	static const int CONDENSED_CHAR_WIDTH = 7;			// pixel width of a character in the condensed font
	static const int WIDTH_IN_PIXELS = WIDTH * CHAR_WIDTH;		// window size in pixels
	static const int HEIGHT_IN_PIXELS = HEIGHT * 9;		// window height in pixels
	static const int MAX_HORIZONTAL_TAB = 12;			// only 12 horizontal tabs on the MX-80
	static const int MAX_VERTICAL_TAB = 8;				// only 8 vertical tabs on the MX-80

private:
	enum {LOW_RES, HIGH_RES};

private:
	#ifdef _WIN32
		HINSTANCE hInstance;
		HWND window;
		HDC bitmap;
	#else
		long hInstance;
		long window;
		long bitmap;
	#endif
	long window_hidden;
	unsigned char *canvas;
	unsigned char *font;
	long cursor_x;
	unsigned char buffer[1024];
	unsigned char *buffer_pos;
	unsigned char *in_esc;
	unsigned long in_graphics;
	long tab[MAX_HORIZONTAL_TAB];
	unsigned char vertical_tab[MAX_VERTICAL_TAB];

	long enlarged, bold, condensed;
	long right_margin, hard_right_margin;
	long line_spacing;
	long form_feed_gap;
	long online;
	long current_line;							// current line number on the page
	long current_char_width;
	long current_language;

protected:
	#ifdef _WIN32
		virtual LRESULT windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
	#else
		virtual long windows_callback(long hwnd, long message, long wParam, long lParam);
	#endif
	void print_char(long pos_x, long pos_y, unsigned char ch);
	void print_graphic(long pos_x, long pos_y, unsigned char bit_pattern, long resolution);
	void paint_text(void);
	#ifdef _WIN32
		void window_paint(HWND window);
	#else
		void window_paint(long window);
	#endif
	void flush_print_buffer(void);
	void reset_vertical_tabs(void);
	unsigned char translate(unsigned char from);

public:
	#ifdef _WIN32
		mx_80(HINSTANCE hInstance);
	#else
		mx_80(long hInstance);
	#endif
	virtual ~mx_80() {};

	long create_window(const char *window_title);
	void putch(unsigned char character);
} ;

#endif /* MX_80_H_ */
