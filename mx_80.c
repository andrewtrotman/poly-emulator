/*
	MX_80.C
	-------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include <stdio.h>
#include <string.h>
#include "resources/resource.h"
#include "mx_80.h"
#include "mx_80_font.h"
#include "dib.h"

#define FALSE 0
#define TRUE !FALSE

/*
	MX_80::MX_80()
	--------------
*/
#ifdef _WIN32
	mx_80::mx_80(HINSTANCE hInstance)
#else
	mx_80::mx_80(long hInstance)
#endif
{
#ifdef _WIN32
	unsigned char tab_stop;

	HGDIOBJ bmp;
	BITMAPINFO256 info;

	this->hInstance = hInstance;
	info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	info.bmiHeader.biWidth = WIDTH_IN_PIXELS;
	info.bmiHeader.biHeight = -HEIGHT_IN_PIXELS;			// -ve for a top-down DIB
	info.bmiHeader.biPlanes = 1;
	info.bmiHeader.biBitCount = 8;
	info.bmiHeader.biCompression = BI_RGB;					// not compressed
	info.bmiHeader.biSizeImage = 0;
	info.bmiHeader.biXPelsPerMeter = 0;
	info.bmiHeader.biYPelsPerMeter = 0;
	info.bmiHeader.biClrUsed = 0;
	info.bmiHeader.biClrImportant = 0;
	memset(info.bmiColors, 0, sizeof(info.bmiColors));
	memcpy(info.bmiColors, pallette, sizeof(pallette));
	bitmap = CreateCompatibleDC(NULL);
	bmp = CreateDIBSection(bitmap, (BITMAPINFO *)&info, DIB_RGB_COLORS, (void **)&canvas, NULL, 0);
	SelectObject(bitmap, bmp);
	font = mx_80_font;
	condensed = FALSE;

	cursor_x = 0;
	buffer_pos = buffer;
	in_esc = NULL;
	in_graphics = 0;
	hard_right_margin = right_margin = 80 * CHAR_WIDTH;
	bold = enlarged = FALSE;
	line_spacing = 9;
	form_feed_gap = 1;
	online = TRUE;
	current_line = 0;
	current_char_width = CHAR_WIDTH;
	current_language = 0;

	reset_vertical_tabs();
	for (tab_stop = 0; tab_stop < MAX_HORIZONTAL_TAB; tab_stop++)
		tab[tab_stop] = tab_stop * 8 * current_char_width;

	window_hidden = TRUE;
#else
#endif
}

/*
	MX_80::RESET_VERTICAL_TABS()
	----------------------------
*/
void mx_80::reset_vertical_tabs(void)
{
long tab_stop;

for (tab_stop = 0; tab_stop < MAX_VERTICAL_TAB; tab_stop++)
	vertical_tab[tab_stop] = (unsigned char)(tab_stop * 2);
}

/*
	WINDOWS_CALLBACK()
	------------------
*/
#ifdef _WIN32
	static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static long windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	mx_80 *object = NULL;

	if (message == WM_CREATE)
		{
		object = (mx_80 *)((CREATESTRUCT *)lParam)->lpCreateParams;
		object->window = hwnd;
		return object->windows_callback(hwnd, message, wParam, lParam);
		}
	else if ((object = (mx_80 *)GetWindowLongPtr(hwnd, GWLP_USERDATA)) != NULL)
		return object->windows_callback(hwnd, message, wParam, lParam);
	else
		return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	MX_80::WINDOWS_CALLBACK()
	-------------------------
*/
#ifdef _WIN32
	LRESULT mx_80::windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	long mx_80::windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	RECT client_size, window_size;

	switch(message)
		{
		/*
			Create Window
		*/
		case WM_CREATE:
			/*
				Set the size of the canvas.
			*/
			GetClientRect(hwnd, &client_size);
			GetWindowRect(hwnd, &window_size);
			SetWindowPos(hwnd, HWND_TOP, window_size.top, window_size.left, WIDTH_IN_PIXELS + (WIDTH_IN_PIXELS - client_size.right), 2 * HEIGHT_IN_PIXELS + (2 * HEIGHT_IN_PIXELS - client_size.bottom), 0);
			return 0;

		/*
			Redraw the window
		*/
		case WM_PAINT:
			window_paint(hwnd);
			return 0;

		/*
			Close the window
		*/
		case WM_QUIT:
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		}
	return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	MX_80::CREATE_WINDOW()
	----------------------
*/
long mx_80::create_window(const char *window_title)
{
#ifdef _WIN32
	HWND window;
	WNDCLASSEX windowClass;

	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_GLOBALCLASS;
	windowClass.lpfnWndProc = ::windows_callback;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = 0;
	windowClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_TERMINAL));
	windowClass.hCursor = NULL;
	windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = "MX80";
	windowClass.hIconSm = NULL;

	RegisterClassEx(&windowClass);

	window = CreateWindowEx(NULL,		// extended style
		"MX80",							// class name
		window_title,					// window name
		WS_EX_OVERLAPPEDWINDOW | WS_BORDER| WS_SYSMENU,			// | WS_VISIBLE,
		120, 120,						// x/y coords
		WIDTH_IN_PIXELS,				// width
		2 * HEIGHT_IN_PIXELS,			// height
		NULL,							// handle to parent
		NULL,							// Menu
		0,								// application instance
		this);							// no extra parameter's

	UpdateWindow(window);
	SetWindowLongPtr(window, GWLP_USERDATA, (LONG_PTR)this);

	return 0;
#else
	return 0;
#endif
}

/*
	MX_80::TRANSLATE()
	------------------
	0 = USA
	1 = France
	2 = Germany
	3 = England
	4 = Denmark
	5 = Sweden
	6 = Italy
	7 = Spain
*/
unsigned char mx_80::translate(unsigned char from)
{
static unsigned char usa[]     = {0x40, 0x5B, 0x5C, 0x5D, 0x7B, 0x7C, 0x7D, 0x7E};
static unsigned char france[]  = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87};
static unsigned char germany[] = {0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F};
static unsigned char denmark[] = {0x40, 0x98, 0x99, 0x9A, 0x9B, 0x9C, 0x9D, 0x7E};
static unsigned char sweden[]  = {0xA1, 0xA2, 0xA3, 0xA4, 0xA7, 0xA8, 0xA9, 0xAA};
static unsigned char italy[]   = {0x40, 0xB0, 0x5C, 0xB1, 0xB3, 0xB4, 0xB5, 0xB6};
static unsigned char spain[]   = {0x40, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0x7D, 0x7E};
unsigned char *language = NULL;

switch (current_language)
	{
	case 0:		// USA
		language = usa;
		break;
	case 1:		// France
		language = france;
		break;
	case 2:		// Germany
		language = germany;
		break;
	case 3:		// England
		return (from == 0x23) ? 0x90 : from;
	case 4:		// Denmark
		language = denmark;
		break;
	case 5:		// Sweden
		switch (from)
			{
			case 0x24:
				return 0xA0;
			case 0x5E:
				return 0xA5;
			case 0x60:
				return 0xA6;
			}
		language = sweden;
		break;
	case 6:		// Italy
		if (from == 0x60)
			return 0xB2;
		language = italy;
		break;
	case 7:		// Spain
		if (from == 0x23)
			return 0xB8;
		language = spain;
		break;
	}

if (language != NULL)
	switch (from)
		{
		case 0x40:
			return language[0];
		case 0x5B:
			return language[1];
		case 0x5C:
			return language[2];
		case 0x5D:
			return language[3];
		case 0x7B:
			return language[4];
		case 0x7C:
			return language[5];
		case 0x7D:
			return language[6];
		case 0x7E:
			return language[7];
		}

return from;
}

/*
	MX_80::PRINT_CHAR()
	-------------------
*/
void mx_80::print_char(long pos_x, long pos_y, unsigned char ch)
{
long from, row, bit;
unsigned char *into, byte;
unsigned char ink = (bold ? PRINTER_BOLD_TEXT_COLOUR : PRINTER_TEXT_COLOUR);

if ((ch &= 0x7F) < ' ')
	ch = ' ';

ch = translate(ch);

from = (long)(ch - ' ') * 18 + 1018;			// 1018 is the location of ' ' and each char takes 18 bytes

for (row = 0; row < 9; row++)
	{
	into = canvas + (pos_x + pos_y * 9 * WIDTH_IN_PIXELS) + row * WIDTH_IN_PIXELS;
	byte = font[from + row];
	for (bit = 7; bit >= 0; bit -= condensed ? 2 : 1)
		if (byte & (1 << bit))
			{
			*into++ = ink;
			if (enlarged)
				*into++ = ink;
			}
		else
			{
			into++;
			if (enlarged)
				into++;
			}

	byte = font[from + row + 9];
	for (bit = 7; bit >= 0; bit -= condensed ? 2 : 1)
		if (byte & (1 << bit))
			{
			*into++ = ink;
			if (enlarged)
				*into++ = ink;
			}
		else
			{
			into++;
			if (enlarged)
				into++;
			}
	}
}

/*
	MX_80::PRINT_GRAPHIC()
	----------------------
*/
void mx_80::print_graphic(long pos_x, long pos_y, unsigned char bit_pattern, long resolution)
{
long row;
unsigned char *into;
unsigned char ink = PRINTER_TEXT_COLOUR;

for (row = 0; row < 8; row++)
	{
	into = canvas + (pos_x + pos_y * 9 * WIDTH_IN_PIXELS) + row * WIDTH_IN_PIXELS;
	if (bit_pattern & (0x80 >> row))
		{
		*into++ = ink;
		if (resolution == LOW_RES)
			*into++ = ink;
		}
	}
}

/*
	MX_80::WINDOW_PAINT()
	---------------------
*/
#ifdef _WIN32
	void mx_80::window_paint(HWND window)
#else
	void mx_80::window_paint(long window)
#endif
{
#ifdef _WIN32
	PAINTSTRUCT paintStruct;
	HDC hDC;

	hDC = BeginPaint(window, &paintStruct);
	StretchBlt(hDC, 0, 0, WIDTH_IN_PIXELS, 2 * HEIGHT_IN_PIXELS, bitmap, 0, 0, WIDTH_IN_PIXELS, HEIGHT_IN_PIXELS, SRCCOPY);
	EndPaint(window, &paintStruct);
#endif
}

/*
	MX_80::FLUSH_PRINT_BUFFER()
	---------------------------
*/
void mx_80::flush_print_buffer(void)
{
unsigned char *current;
long esc = FALSE;
long tab_stop, possible;
long bytes, which_byte, mode;

if (window_hidden)
	{
	window_hidden = FALSE;
	#ifdef _WIN32
		ShowWindow(window, SW_SHOW);
	#endif
	}

for (current = buffer; current < buffer_pos; current++)
	{
	switch (*current)
		{
		case 0x07:			// Bell
			#ifdef _WIN32
				Beep(262, 250);	// middle C for 1/4 second
			#endif
			break;
		case 0x09:			// Horizontal Tab
			for (tab_stop = 0; tab_stop < MAX_HORIZONTAL_TAB; tab_stop++)
				if (tab[tab_stop] > cursor_x)
					{
					cursor_x = tab[tab_stop];
					break;
					}
			break;
		case 0x0E:		// Shift Out (start enlarged font)
			enlarged = TRUE;
			break;
		case 0x12:		// Device Control 2 (turn off condensed printing)
			condensed = FALSE;
			current_char_width = CHAR_WIDTH;
			break;
		case 0x14:		// Device Control 4 (turn off enlarged printing)
			enlarged = FALSE;
			break;
		case ESC:
			current++;
			if (current < buffer_pos)
				switch (*current)
					{
					case 0x0E:		// Shift Out (start enlarged font)
						enlarged = TRUE;
						break;
					case 0x0F:		// Shift In (start condenset font)
						break;
					case 'A':				// set scroll (in 72th of an inch (pixels))
						current++;
						if (current < buffer_pos)
							line_spacing = *current & 0x7F;
						if (line_spacing > 85)
							line_spacing = 85;
						if (line_spacing <= 0)
							line_spacing = 1;
						break;
					case 'B':				// set vertical tabstops
						current++;
						for (tab_stop = 0; current < buffer_pos && *current != 0 && tab_stop < MAX_VERTICAL_TAB; tab_stop++)
							vertical_tab[tab_stop++] = *current++;
						if (tab_stop == 0)
							reset_vertical_tabs();
						break;
					case 'C':				// set page length
						reset_vertical_tabs();
						form_feed_gap = 1;
						current++;
						if (current < buffer_pos)
							if (*current == 0)
								current++;
						break;
					case 'D':				// set tabstops
						current++;
						for (tab_stop = 0; current < buffer_pos && *current != 0 && tab_stop < MAX_HORIZONTAL_TAB; tab_stop++)
							if ((possible = *current++ * current_char_width) <= hard_right_margin)
								tab[tab_stop++] = possible;
						break;
					case 'E':				// Emphasized characters
						break;
					case 'F':
						bold = FALSE;
						break;
					case 'K':
					case 'L':
						mode = *current == 'K' ? LOW_RES : HIGH_RES;
						current++;
						bytes = *current++;
						bytes |= *current << 8;
						for (which_byte = 0; which_byte < bytes && current < buffer_pos; which_byte++)
							{
							current++;
							print_graphic(cursor_x, HEIGHT - 1, *current, mode);
							cursor_x += mode == LOW_RES ? 2 : 1;
							}
						break;
					case 'N':				// number of lines to skip at the perforations of the paper
						current++;
						if (current < buffer_pos)
							form_feed_gap = *current;
						break;
					case 'O':				// reset form feed gap
						form_feed_gap = 0;
						break;
					case 'Q':				// Set right margin
						current++;
						if (current < buffer_pos)
							possible = *current * current_char_width;
						if (possible <= hard_right_margin)
							right_margin = possible;
						break;
					case 'R':
						current++;
						if (current < buffer_pos)
							current_language = *current;
						break;
					case '0':				// set scroll to 1/8 inch
						line_spacing = 9;
						break;
					case '2':				// set scroll to 1/6 inch
						line_spacing = 12;
						break;
					case '8':				// ignore paper end detector
						break;
					case '9':				// ignore ignore paper end detector
						break;
					}
			break;
		default:
			if (*current < ' ' || *current > 0x7F)
				break;

			print_char(cursor_x, HEIGHT - 1, *current);
			cursor_x += current_char_width;
			if (enlarged)
				cursor_x += current_char_width;
			if (cursor_x > right_margin)
				cursor_x = 0;
			if (cursor_x > hard_right_margin)
				cursor_x = 0;
			break;
		}
	}
buffer_pos = buffer;
#ifdef _WIN32
	RedrawWindow(window, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
#endif
}

/*
	MX_80::PUTCH()
	--------------
*/
void mx_80::putch(unsigned char character)
{
unsigned char *into;
long current, tab_stop, dotted_line;

if (!online)
	{
	if (character == 0x11)		// Device Control 1 (select printer)
		online = TRUE;
	}
else if (in_graphics != 0)
	{
	in_graphics--;
	*buffer_pos++ = character;
	}	
else if (in_esc != NULL)
	{
	*buffer_pos++ = character;
	switch (*(in_esc + 1))
		{
		case 0x0F:
			current_char_width = CONDENSED_CHAR_WIDTH;
			condensed = TRUE;
			break;
		case 0x0E:
		case 'B':
		case 'D':
			break;
		case 'A':
		case 'N':
		case 'Q':
		case 'R':
			if (buffer_pos - in_esc == 3)
				in_esc = NULL;
			break;
		case 'C':
			if (buffer_pos - in_esc == 3)
				if (in_esc[2] != 0)
					in_esc = NULL;				// ESC C n		(page length is n lines)
			if (buffer_pos - in_esc > 3)
				in_esc = NULL;					// ESC C [0] m  (page length is m inches)
			break;
		case 'K':
			if (buffer_pos - in_esc == 4)		// ESC K m n  and ESC L m n	(graphics)
				{
				in_graphics = (unsigned long)in_esc[2] | ((unsigned long)in_esc[3] << 8);
				in_esc = NULL;
				}
			break;
		case 'E':
			bold = TRUE;
			in_esc = NULL;
			break;
		case 'F':
		case 'O':
		case '0':
		case '2':
		case '8':
		case '9':
		default:
			in_esc = NULL;			// drop out of ESC mode
			break;
		}
	if (character == 0)
		in_esc = NULL;
	}
else
	switch (character)
		{
		case 0x08:		// Backspace
			if (buffer_pos > buffer)
				buffer_pos--;
			break;
		case 0x0A:		// LF
			flush_print_buffer();
			memmove(canvas, canvas + line_spacing * WIDTH_IN_PIXELS, WIDTH_IN_PIXELS * HEIGHT_IN_PIXELS - line_spacing * WIDTH_IN_PIXELS);
			memset((canvas + HEIGHT_IN_PIXELS * WIDTH_IN_PIXELS) - line_spacing *  WIDTH_IN_PIXELS, BACKGROUND_COLOUR, line_spacing *  WIDTH_IN_PIXELS);
			current_line++;
			break;
		case 0x0B:		// VT
			flush_print_buffer();
			cursor_x = 0;

			for (tab_stop = 0; tab_stop < MAX_VERTICAL_TAB; tab_stop++)
				if (vertical_tab[tab_stop] > current_line)
					{
					while (current_line < vertical_tab[tab_stop])
						{
						memmove(canvas, canvas + line_spacing * WIDTH_IN_PIXELS, WIDTH_IN_PIXELS * HEIGHT_IN_PIXELS - line_spacing * WIDTH_IN_PIXELS);
						memset((canvas + HEIGHT_IN_PIXELS * WIDTH_IN_PIXELS) - line_spacing *  WIDTH_IN_PIXELS, BACKGROUND_COLOUR, line_spacing *  WIDTH_IN_PIXELS);
						current_line++;
						}
					break;
					}
			break;
		case 0x0C:		// FF
			flush_print_buffer();
			dotted_line = form_feed_gap >> 1;
			for (current = 0; current < form_feed_gap; current++)
				{
				if (current == dotted_line)
					{
					into = (canvas + HEIGHT_IN_PIXELS * WIDTH_IN_PIXELS) - (line_spacing + 1) *  WIDTH_IN_PIXELS;
					for (current = 0; current < WIDTH_IN_PIXELS; current += 8)
						{
						*into++ = PRINTER_PERFORATION_COLOUR;
						*into++ = PRINTER_PERFORATION_COLOUR;
						*into++ = PRINTER_PERFORATION_COLOUR;
						*into++ = PRINTER_PERFORATION_COLOUR;
						*into++ = 0;
						*into++ = 0;
						*into++ = 0;
						*into++ = 0;
						}
					}
				memmove(canvas, canvas + line_spacing * WIDTH_IN_PIXELS, WIDTH_IN_PIXELS * HEIGHT_IN_PIXELS - line_spacing * WIDTH_IN_PIXELS);
				memset((canvas + HEIGHT_IN_PIXELS * WIDTH_IN_PIXELS) - line_spacing *  WIDTH_IN_PIXELS, BACKGROUND_COLOUR, line_spacing *  WIDTH_IN_PIXELS);
				}
			cursor_x = 0;
			current_line = 0;
			break;
		case 0x0D:		// CR
			flush_print_buffer();
			cursor_x = 0;
			break;
		case 0x0F:		// Shift In (turn on condensed font)
			current_char_width = CONDENSED_CHAR_WIDTH;
			condensed = TRUE;
			break;
		case ESC:
			in_esc = buffer_pos;
			*buffer_pos++ = character;
			break;
		case 0x11:		// Device Control 1 (select printer)
			buffer_pos = buffer;
			break;
		case 0x13:		// Decice Control 3 (deselect printer)
			online = FALSE;
			break;
		default:
			*buffer_pos++ = character;
			break;
		}
}
