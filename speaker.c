/*
	SPEAKER.C
	---------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include <stdio.h>
#include <math.h>
#include "speaker.h"

#define SAMPLE_RATE 11025
#define PI 3.14159

/*
	SPEAKER::SPEAKER()
	------------------
*/
speaker::speaker()
{
#ifdef _WIN32
	WAVEFORMATEX waveformat;

	waveformat.wFormatTag = WAVE_FORMAT_PCM;
	waveformat.nChannels = 1;
	waveformat.nSamplesPerSec = SAMPLE_RATE;
	waveformat.nAvgBytesPerSec = SAMPLE_RATE;
	waveformat.nBlockAlign = 1;
	waveformat.wBitsPerSample = 8;
	waveformat.cbSize = 0;

	waveOutOpen(&hWaveOut, WAVE_MAPPER, &waveformat, 0, 0, CALLBACK_NULL);

	buffer_size = SAMPLE_RATE;
	buffer = new unsigned char [buffer_size];

	header.dwBufferLength = buffer_size;
	header.dwBytesRecorded = 0;
	header.dwUser = 0;
	header.dwFlags = WHDR_BEGINLOOP | WHDR_ENDLOOP;
	header.dwLoops = 1000;
	header.lpNext = NULL;
	header.reserved = 0;
	header.lpData = (char *)buffer;

	angle = 0;
	frequency = 262;
	fill_buffer(buffer);
	waveOutPrepareHeader(hWaveOut, &header, sizeof(header));
	waveOutWrite(hWaveOut, &header, sizeof(header));
	waveOutPause(hWaveOut);
	frequency = 0;
#endif
}

/*
	SPEAKER::~SPEAKER()
	-------------------
*/
speaker::~speaker()
{
#ifdef _WIN32
	quiet();
	waveOutClose(hWaveOut);
	delete [] buffer;
#endif
}

/*
	SPEAKER::FILL_BUFFER()
	----------------------
*/
void speaker::fill_buffer(unsigned char *buffer)
{
long which;

for (which = 0; which < buffer_size; which++)
	{
	buffer[which] = (unsigned char)(127 + 127 * sin(angle));

	angle += 2 * PI * frequency / SAMPLE_RATE;

	if (angle > 2 * PI)
		angle -= 2 * PI;
	}
}

/*
	SPEAKER::PLAY()
	---------------
*/
void speaker::play(long frequency)
{
#ifdef _WIN32
if (this->frequency != frequency)
	{
	waveOutReset(hWaveOut);

	this->frequency = frequency;

	fill_buffer(buffer);
	header.dwBufferLength = buffer_size;
	header.dwLoops = 1000;
	header.lpData = (char *)buffer;

	waveOutWrite(hWaveOut, &header, sizeof(header));
	}
#endif
}

/*
	SPEAKER::QUIET()
	----------------
*/
void speaker::quiet(void)
{
#ifdef _WIN32
	frequency = 0;
	waveOutPause(hWaveOut);
#endif
}

