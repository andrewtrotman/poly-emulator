/*
	PROTEUS_UNIT.C
	--------------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include "proteus_unit.h"
#include "resources/resource.h"

proteus *server;

/*
	Z80_MEMORY_READ()
	-----------------
*/
byte z80_memory_read(word addr)
{
return server->memory[addr];
}

/*
	Z80_MEMORY_WRITE()
	------------------
*/
void z80_memory_write(word addr, byte val)
{
server->memory[addr] = val;
}

/*
	Z80_IO_READ()
	-------------
*/
byte z80_io_read(word addr)
{
return server->read(0xE000 + (addr & 0xFF));
}

/*
	Z80_IO_WRITE()
	--------------
*/
void z80_io_write(word addr, byte val)
{
return server->write(0xE000 + (addr & 0xFF), val);
}

/*
	PROTEUS_UNIT::PROTEUS_UNIT()
	----------------------------
*/
#ifdef _WIN32
	proteus_unit::proteus_unit(HINSTANCE hInstance)
#else
	proteus_unit::proteus_unit(long hInstance)
#endif
{
#ifdef _WIN32
	BITMAP bitmap;

	this->hInstance = hInstance;

	bmp_about = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_PROTEUS_PEOPLE));
	GetObject(bmp_about, sizeof(bitmap), &bitmap);
	bmp_about_width = bitmap.bmWidth;
	bmp_about_height = bitmap.bmHeight;

	bmp_proteus = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_PROTEUS));
	GetObject(bmp_proteus, sizeof(bitmap), &bitmap);
	bmp_proteus_width = bitmap.bmWidth;
	bmp_proteus_height = bitmap.bmHeight;
#endif
}

/*
	PROTEUS_UNIT::~PROTEUS_UNIT()
	-----------------------------
*/
proteus_unit::~proteus_unit()
{
#ifdef _WIN32
	DeleteObject(bmp_about);
#endif
}

/*
	ABOUTDLGPROC()
	--------------
*/
#ifdef _WIN32
	static INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static long AboutDlgProc(long hDlg, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	proteus_unit *object = NULL;

	if (message == WM_INITDIALOG)
		SetWindowLongPtr(hDlg, GWLP_USERDATA, (LONG_PTR)lParam);

	if ((object = (proteus_unit *)GetWindowLongPtr(hDlg, GWLP_USERDATA)) != NULL)
		return object->about_box(hDlg, message, wParam, lParam);
	else
		return FALSE;
#else
	return FALSE;
#endif
}

/*
	PROTEUS_UNIT::ABOUT_BOX()
	-------------------------
*/
#ifdef _WIN32
	INT_PTR CALLBACK proteus_unit::about_box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
#else
	bool proteus_unit::about_box(long hDlg, long message, long wParam, long lParam)
#endif
{
	#ifdef _WIN32
	HWND button;
	HDC hdcMem;
	HGDIOBJ hbmOld;
	RECT client_size, window_size, button_size, button_window_size;

	switch (message)
		{
		case WM_ERASEBKGND:
			hdcMem = CreateCompatibleDC((HDC)wParam);
			hbmOld = SelectObject(hdcMem, bmp_about);
			BitBlt((HDC)wParam, 0, 0, bmp_about_width, bmp_about_height, hdcMem, 0, 0, SRCCOPY);
			StretchBlt((HDC)wParam, 0, bmp_about_height - 1, bmp_about_width, bmp_about_height, hdcMem, 0, 0, 1, 1, SRCCOPY);
			DeleteDC(hdcMem);
			return TRUE;
		case WM_INITDIALOG:
			/*
				Set the size of the dialog box
			*/
			GetClientRect(hDlg, &client_size);
			GetWindowRect(hDlg, &window_size);
			button = GetDlgItem(hDlg, IDOK);
			GetClientRect(button, &button_size);
			SetWindowPos(hDlg, HWND_TOP, window_size.left, window_size.top, bmp_about_width + (window_size.right - window_size.left - client_size.right), bmp_about_height + (window_size.bottom  - window_size.top - client_size.bottom) + button_size.bottom, 0);

			/*
				Now move the button
			*/
			GetClientRect(hDlg, &client_size);			// This has change as a consequence of the SetWindowPos() call above
			GetWindowRect(button, &button_window_size);
			MoveWindow(button, (client_size.right - (button_window_size.right - button_window_size.left)) / 2, client_size.bottom - button_size.bottom - (window_size.bottom - button_window_size.bottom) / 2, button_size.right, button_size.bottom, TRUE);
			return TRUE;
		case WM_CTLCOLORDLG: 
			return (BOOL)(GetStockObject(WHITE_BRUSH) != NULL);
		case WM_COMMAND:
			EndDialog(hDlg, 0);			// OK button
			return TRUE;
		}
#endif
return FALSE;
}

/*
	PROTEUS_UNIT::LOAD_DISK()
	-------------------------
*/
#ifdef _WIN32
	void proteus_unit::load_disk(proteus *server, HWND hwnd, long menu)
#else
	void proteus_unit::load_disk(proteus *server, long hwnd, long menu)
#endif
{
#ifdef _WIN32
	unsigned char *disk_name;
	char menu_name[1024];
	long drive, error_code;

	switch (menu)
		{
		case ID_DRIVE_0:
			drive = 0;
			break;
		case ID_DRIVE_1:
			drive = 1;
			break;
		case ID_DRIVE_2:
			drive = 2;
			break;
		case ID_DRIVE_3:
			drive = 3;
			break;
		}

	disk_name = server->fdc[drive]->mount_disk(&error_code);
	if (disk_name == NULL)
		{
		sprintf(menu_name, "%d: Empty", drive);
		if (error_code == wd1771::ERROR_UNKNOWN_FORMAT)
			MessageBox(NULL, "Invalid POLYSYS, FLEX or CP/M disk image", "Error Mounting Disk", MB_OK);
		else if (error_code == wd1771::ERROR_CANNOT_STAT || error_code == wd1771::ERROR_CANNOT_READ)
			MessageBox(NULL, "File I/O Error", "Error Mounting Disk", MB_OK);
		}
	else
		sprintf(menu_name, "%d:%s", drive, disk_name);

	ModifyMenu(GetMenu(hwnd), menu, MF_BYCOMMAND | MF_STRING, menu, menu_name);
#endif
}

/*
	PROTEUS_UNIT::LOAD_ROM()
	------------------------
*/
unsigned char *proteus_unit::load_rom(long version)
{
#ifdef _WIN32
	unsigned char *answer;
	HMENU menubar = GetMenu(window);

	CheckMenuItem(menubar, ID_ROMS_030982, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(menubar, ID_ROMS_300986, MF_BYCOMMAND | MF_UNCHECKED);
	switch (version)
		{
		case ID_ROMS_030982:
			answer = server->load_rom(30982);
			break;
		case ID_ROMS_300986:
			answer = server->load_rom(300986);
			break;
		default:
			answer = server->load_rom(30982);
			break;
		}
	CheckMenuItem(menubar, version, MF_BYCOMMAND | MF_CHECKED);
	DrawMenuBar(window);
	return answer;
#else
	return server->load_rom(30982);
#endif
}

/*
	PROTEUS_UNIT::WINDOWS_CALLBACK()
	--------------------------------
*/
#ifdef _WIN32
	LRESULT proteus_unit::windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	long proteus_unit::windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	long long cycles_spent, cycles_to_spend;
	RECT client_size, window_size;
	PAINTSTRUCT paintStruct;
	HDC hDC, hdcMem; 

	switch(message)
		{
		case WM_CREATE:
			this->window = hwnd;
			GetClientRect(hwnd, &client_size);
			GetWindowRect(hwnd, &window_size);
			SetWindowPos(hwnd, HWND_TOP, window_size.left, window_size.top, bmp_proteus_width + (window_size.right - window_size.left - client_size.right), bmp_proteus_height + (window_size.bottom  - window_size.top - client_size.bottom), 0);
			return 0;

		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		case WM_PAINT:
			hDC = BeginPaint(hwnd,&paintStruct);
			hdcMem = CreateCompatibleDC(hDC);
			SelectObject(hdcMem, bmp_proteus);
			BitBlt(hDC, 0, 0, bmp_proteus_width, bmp_proteus_height, hdcMem, 0, 0, SRCCOPY);
			DeleteDC(hdcMem);
			EndPaint(hwnd, &paintStruct);
			return 0;

		case WM_TIMER:
			/*
				Run for a 10th of an emulated second (clock speed / 10)
			*/
			cycles_spent = server->cycles; 
			cycles_to_spend = server->cpu_frequency / 10;
			while (server->cycles < cycles_spent + cycles_to_spend)
				server->step();
			return 0;

		case WM_COMMAND:
			switch (LOWORD(wParam))
				{
				/*
					FILE MENU
				*/
				case ID_FILE_RESET:
					server->reset();
					break;

				case ID_FILE_EXIT:
					PostQuitMessage(0);
					break;

				/*
					DRIVE MENU
				*/
				case ID_DRIVE_0:
				case ID_DRIVE_1:
				case ID_DRIVE_2:
				case ID_DRIVE_3:
					load_disk(server, hwnd, LOWORD(wParam));
					break;

				case ID_ROMS_030982:
				case ID_ROMS_300986:
					load_rom(LOWORD(wParam));
					break;

				/*
					HELP MENU
				*/
				case ID_HELP_ABOUT_PROTEUS:
					DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_ABOUT_PROTEUS), hwnd, AboutDlgProc, (LPARAM)this);
					break;
				}
			return 0;
		}

	return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	WINDOWS_CALLBACK()
	------------------
*/
#ifdef _WIN32
	static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static long windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	proteus_unit *object = NULL;

	if (message == WM_CREATE)
		{
		object = (proteus_unit *)((CREATESTRUCT *)lParam)->lpCreateParams;
		return object->windows_callback(hwnd, message, wParam, lParam);
		}
	else if ((object = (proteus_unit *)GetWindowLongPtr(hwnd, GWLP_USERDATA)) != NULL)
		return object->windows_callback(hwnd, message, wParam, lParam);
	else
		return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	PROTEUS_UNIT::CREATE_WINDOW()
	-----------------------------
*/
proteus *proteus_unit::create_window(const char *window_title, long mode)
{
#ifdef _WIN32
	WNDCLASSEX windowClass;
	HWND hwnd;

	this->hInstance = hInstance;
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = ::windows_callback;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = 0;
	windowClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_PROTEUS));
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = "ProteusWindow";
	windowClass.hIconSm = 0;

	RegisterClassEx(&windowClass);
	hwnd = CreateWindowEx(NULL, "ProteusWindow", window_title, WS_EX_OVERLAPPEDWINDOW | WS_BORDER | WS_VISIBLE | WS_SYSMENU, 100, 100, 400, 400, NULL, LoadMenu(0, MAKEINTRESOURCE(IDR_MENU_PROTEUS)), 0, this);
	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)this);

	server = new proteus(hInstance, mode == ACTIVE ? proteus::ACTIVE_TERMINAL : proteus::PASSIVE_TERMINAL);
	server->reset();
	load_rom(ID_ROMS_030982);

	/*
		FIX THIS
	*/
	::server = server;

	if (mode == ACTIVE)
		{
		/*
			The timing loop is done by the Poly in the case where we have both in a network
		*/
		SetTimer(hwnd, 0, 100, NULL);			// run for a 10th of a second every 100 miliseconds
		}

	return server;
#else
	server = new proteus(hInstance, mode == ACTIVE ? proteus::ACTIVE_TERMINAL : proteus::PASSIVE_TERMINAL);
	server->reset();
	load_rom(ID_ROMS_030982);

	::server = server;
	return server;
#endif

}
