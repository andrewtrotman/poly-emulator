/*
	TYPEDEFS.H
	----------
*/
#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int dword;

#endif /* TYPEDEFS_H_ */
