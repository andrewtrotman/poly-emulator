/*
	MC6821.C
	--------
*/
#include "mc6821.h"

/*
	MC6821::MC6821()
	----------------
*/
mc6821::mc6821()
{
byte_a = byte_b = 0;
cra = crb = dda = ddb = 0;
}

/*
	MC6821::READ()
	--------------
*/
byte mc6821::read(word address)
{
byte answer;

switch (address)
	{
	case 0x0000:
		if ((cra & 0x04) == 0)
			answer = dda;
		else
			{
			d_irqa();
			answer = in_a(); 		// should this be: answer = in_a() & ~dda;
			}
		return answer;

	case 0x0001:
		return cra;

	case 0x0002:
		if ((crb & 0x04) == 0)
			answer = ddb;
		else
			{
			d_irqb();
			answer = in_b();		// should this be: answer = in_b() & ~ddb;
			}
		return answer;

	case 0x0003:
		return crb;

	default:
		return 0;
	}
}

/*
	MC6821::WRITE()
	---------------
*/
void mc6821::write(word address, byte value)
{
switch (address)
	{
	case 0x0000:
		if ((cra & 0x04) == 0)			// Data Direction Access Control
			dda = value;
		else
			out_a(value & dda);
		break;

	case 0x0001:
		cra = value & 0x3F;		// we can only write to bits 0..5
		break;

	case 0x0002:
		if ((crb & 0x04) == 0)			// Data Direction Access Control
			ddb = value;
		else
			out_b(value & ddb);
		break;

	case 0x0003:
		crb = value & 0x3F;		// we can only write to bits 0..5
		break;
	}
}

/*
	MC6821::ARRIVED_A()
	-------------------
	Call this if data arrives at port A
*/
void mc6821::arrived_a(byte value, byte ca1, byte ca2) 
{ 
byte_a = value;
if ((ca1 != 0) && (cra & 0x01))
	irqa1();
if ((ca2 != 0) && (cra & 0x20) && (cra & 0x08))
	irqa2();
}

/*
	MC6821::ARRIVED_B()
	-------------------
	Call this if data arrives at port B
*/
void mc6821::arrived_b(byte value, byte cb1, byte cb2) 
{ 
byte_b = value;
if ((cb1 != 0) && (crb & 0x01))
	irqb1();
if ((cb2 != 0) && (crb & 0x20) && (crb & 0x08))
	irqb2();
}
