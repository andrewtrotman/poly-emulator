/*
	MAIN_FOR_POLY.C
	---------------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include "poly_unit.h"
#include "proteus_unit.h"

#define MAX_POLY_IN_NETWORK 10
#define POLY_IN_NETWORK 1

static const  char *poly_unit_name[] = {
	"First Poly",
	"Second Poly",
	"Third Poly",
	"Fourth Poly",
	"Fifth Poly",
	"Sixth Poly",
	"Seventh Poly",
	"Eighth Poly",
	"Ninth Poly",
	"Last Poly"
	} ;

#ifdef _WIN32
	/*
		WINMAIN()
		---------
	*/
	int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	{
	MSG msg;
	long done;
	#if POLY_IN_NETWORK != 1
		long current_poly;
	#endif
	poly_unit *client;
	poly **current, *client_list[MAX_POLY_IN_NETWORK + 1];
	proteus_unit *server;
	proteus *drive;
	poly *downstream;

	current = client_list;

	server = new proteus_unit(hInstance);
	drive = server->create_window("Proteus", proteus_unit::PASSIVE);
	#if POLY_IN_NETWORK == 1
		client = new poly_unit(hInstance, drive);
		downstream = client->connect_to(drive);
		client->create_window("Poly");
	#else
		client = new poly_unit(hInstance);
		*current++ = downstream = client->connect_to(drive);
		*current = NULL;
		client->create_window(poly_unit_name[0], poly_unit::PASSIVE);

		for (current_poly = 1; current_poly < POLY_IN_NETWORK - 1; current_poly++)
			{
			client = new poly_unit(hInstance);
			*current++ = downstream = client->connect_to(downstream);
			*current = NULL;
			client->create_window(poly_unit_name[current_poly], poly_unit::PASSIVE);
			}

		client = new poly_unit(hInstance, drive, client_list);
		downstream = client->connect_to(downstream);
		client->create_window(poly_unit_name[9]);
	#endif

	done = false;
	while(!done)
		{
		PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE);		// peek but don't remove the message from the queue (that happens below)
		if (msg.message == WM_QUIT)
			done = true;
		else
			{
			GetMessage(&msg, NULL, 0, 0);
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}
		}

	return msg.wParam;
	}
#else
	int main(void)
	{
	poly_unit *client;
	poly **current, *client_list[MAX_POLY_IN_NETWORK + 1];
	proteus_unit *server;
	proteus *drive;
	poly *downstream;

	current = client_list;

	server = new proteus_unit(0);
	drive = server->create_window("Proteus", proteus_unit::PASSIVE);
	client = new poly_unit(0, drive);
	downstream = client->connect_to(drive);
	client->create_window("Poly");
	return 0;
	}
#endif