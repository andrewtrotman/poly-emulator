/*
	MC6840_WITH_SPEAKER.H
	---------------------
*/
#ifndef MC6840_WITH_SPEAKER_H_
#define MC6840_WITH_SPEAKER_H_

#include "mc6840_with_irq.h"
#include "speaker.h"


/*
	class MC6840_WITH_SPEAKER
	-------------------------
*/
class mc6840_with_speaker : public mc6840_with_irq
{
protected:
	speaker *player;

public:
	mc6840_with_speaker(poly *cpu);
	virtual ~mc6840_with_speaker();

	virtual void write(word address, byte value);
	virtual void reset(void);
} ;

#endif /* MC6840_WITH_SPEAKER_H_ */
