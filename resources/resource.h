//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by resources.rc
//
#define IDR_MENU_POLY                   101
#define IDR_MENU_PROTEUS                103
#define IDD_ABOUT_POLY                  104
#define IDD_ABOUT_PROTEUS               105
#define IDI_ICON1                       106
#define IDI_ICON_POLY                   106
#define IDB_PROTEUS                     107
#define IDB_BITMAP1                     108
#define IDB_PROTEUS_PEOPLE              108
#define IDI_ICON_PROTEUS                109
#define IDB_POLY_PEOPLE                 110
#define IDI_ICON2                       111
#define IDI_ICON_TERMINAL               111
#define ID_FILE_EXIT                    40001
#define ID_FILE_RESET                   40002
#define ID_DRIVE_0                      40005
#define ID_DRIVE_1                      40006
#define ID_DRIVE_2                      40007
#define ID_DRIVE_3                      40008
#define ID_KEYS_BACK                    40024
#define ID_KEYS_REPEAT                  40025
#define ID_KEYS_NEXT                    40026
#define ID_KEYS_EXIT                    40027
#define ID_KEYS_CALC                    40028
#define ID_KEYS_HELP                    40029
#define ID_KEYS_CHARINSERT              40030
#define ID_KEYS_CHARDELETE              40031
#define ID_KEYS_LINEINSERT              40032
#define ID_KEYS_LINEDELETE              40033
#define ID_KEYS_UP                      40034
#define ID_KEYS_DOWN                    40035
#define ID_KEYS_LEFT                    40036
#define ID_KEYS_RIGHT                   40037
#define ID_KEYS_U0                      40038
#define ID_KEYS_U1                      40039
#define ID_KEYS_U2                      40040
#define ID_KEYS_U3                      40041
#define ID_KEYS_U4                      40042
#define ID_KEYS_U5                      40043
#define ID_KEYS_I6                      40044
#define ID_KEYS_U7                      40045
#define ID_KEYS_U6                      40046
#define ID_KEYS_U8                      40047
#define ID_KEYS_U9                      40048
#define ID_KEYS_PAUSE                   40051
#define ID_HELP_ABOUT_POLY              40053
#define ID_HELP_ABOUT_PROTEUS           40054
#define ID_FILE_WARM_RESET              40055
#define ID_ROMS_BASIC_23                40056
#define ID_ROMS_BASIC_30                40057
#define ID_ROMS_BASIC_34                40058
#define ID_KEYS_DOT                     40059
#define ID_KEYS_SHIFT_PAUSE             40060
#define ID_DRIVE                        40062
#define ID_ROMS_BASICV2                 40063
#define ID_ROMS_BASIC_31                40064
#define ID_ROMS_300986                  40065
#define ID_ROMS_030982                  40066

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40067
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
