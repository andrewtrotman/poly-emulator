/*
	MAIN_FOR_POLY.C
	---------------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include "poly_unit.h"

/*
	WINMAIN()
	---------
*/
#ifdef _WIN32
	int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	{
	MSG msg;
	long done;
	poly_unit *client;

	client = new poly_unit(hInstance);
	client->create_window("Poly Unit Available");

	done = false;
	while(!done)
		{
		PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE);		// peek but don't remove the message from the queue (that happens below)
		if (msg.message == WM_QUIT)
			done = true;
		else
			{
			GetMessage(&msg, NULL, 0, 0);
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}
		}

	return msg.wParam;
	}
#else
	int main(void)
	{
	poly_unit *client;

	client = new poly_unit(0);
	client->create_window("Poly Unit Available");

	return 0;
	}
#endif
