/*
	PROTEUS.H
	---------
*/
#ifndef PROTEUS_H_
#define PROTEUS_H_

#ifdef _WIN32
	#include <windows.h>
#endif
#include "mc6809.h"
#include "mc6821.h"
#include "mc6840.h"
#include "mc6850.h"
#include "mc6854.h"
#include "wd1771.h"

/*
	PROTEUS
	-------
*/
class proteus : public mc6809
{
public:
	enum { CPU_6809, CPU_Z80 };
	enum { ACTIVE_TERMINAL, PASSIVE_TERMINAL };

public:
	#ifdef _WIN32
		HINSTANCE hInstance;
	#else
		long hInstance;
	#endif
	long cpu_active;
	long cpu_frequency;
	mc6850 *acia1, *acia2, *acia3;
	mc6821 *pia;
	mc6840 *timer;
	mc6854 *network;
	wd1771 *fdc[4];
	unsigned char drive_select;
	unsigned char *current_rom;

	char z80_eprom_shadow[0x1000];

public:
	virtual byte read(word address);
	virtual void write(word address, byte val);

public:
	#ifdef _WIN32
		proteus(HINSTANCE hInstance, long mode = PASSIVE_TERMINAL);
	#else
		proteus(long hInstance, long mode = PASSIVE_TERMINAL);
	#endif
	~proteus() {}

	virtual void step(void);
	virtual void reset(void);

	virtual void timer_irq(void) {}
	virtual void timer_d_irq(void) {}
	virtual void keyboard_irq(void) {}
	virtual void keyboard_d_irq(void) {}
	virtual void network_irq(void) {}
	virtual void network_d_irq(void) {}

	virtual unsigned char *load_rom(long version);
} ;

#endif /* PROTEUS_H_ */


