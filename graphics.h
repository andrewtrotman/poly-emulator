/*
	GRAPHICS.H
	----------
*/
#ifndef GRAPHICS_H_
#define GRAPHICS_H_

/*
	class GRAPHICS
	--------------
*/
class graphics
{
public:
	graphics() {}
	virtual ~graphics() {}

	void render_page(unsigned char *canvas, unsigned char *memory, unsigned char mix_colour);
	void render_mixed_pages(unsigned char *canvas, unsigned char *screen2, unsigned char *screen4);
	void render_combined_pages(unsigned char *canvas, unsigned char *screen2, unsigned char *screen4);
} ;

#endif /* GRAPHICS_H_ */
