/*
	TERMINAL.C
	----------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include <stdio.h>
#include <string.h>
#include "resources/resource.h"
#include "terminal.h"
#include "saa5050_font.h"
#include "dib.h"

#ifndef FALSE
	#define FALSE 0
#endif
#ifndef TRUE
	#define TRUE !FALSE
#endif

/*
	TERMINAL::TERMINAL()
	--------------------
*/
#ifdef _WIN32
	terminal::terminal(HINSTANCE hInstance)
#else
	terminal::terminal(long hInstance)
#endif
{
#ifdef _WIN32
	HGDIOBJ bmp;
	BITMAPINFO256 info;

	this->hInstance = hInstance;
	info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	info.bmiHeader.biWidth = WIDTH_IN_PIXELS;
	info.bmiHeader.biHeight = -HEIGHT_IN_PIXELS;			// -ve for a top-down DIB
	info.bmiHeader.biPlanes = 1;
	info.bmiHeader.biBitCount = 8;
	info.bmiHeader.biCompression = BI_RGB;		// not compressed
	info.bmiHeader.biSizeImage = 0;
	info.bmiHeader.biXPelsPerMeter = 0;
	info.bmiHeader.biYPelsPerMeter = 0;
	info.bmiHeader.biClrUsed = 0;
	info.bmiHeader.biClrImportant = 0;
	memset(info.bmiColors, 0, sizeof(info.bmiColors));
	memcpy(info.bmiColors, pallette, sizeof(pallette));		// copy the Poly's colour pallette
	bitmap = CreateCompatibleDC(NULL);
	bmp = CreateDIBSection(bitmap, (BITMAPINFO *)&info, DIB_RGB_COLORS, (void **)&canvas, NULL, 0);
	SelectObject(bitmap, bmp);
	font = saa5050_font;

	memset(keyboard_buffer, 0, sizeof(keyboard_buffer));
	memset(screen_buffer, ' ', sizeof(screen_buffer));
	cursor_x = cursor_y = 0;

	keyboard_buffer_read_pos = keyboard_buffer_write_pos = keyboard_buffer;
	flash_state = 0;
	last_4_keys = 0;
	inverse_video = FALSE;
	hidden = TRUE;
	auto_nl = FALSE;
#endif
}

/*
	WINDOWS_CALLBACK()
	------------------
*/
#ifdef _WIN32
	static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static long windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	terminal *object = NULL;

	if (message == WM_CREATE)
		{
		object = (terminal *)((CREATESTRUCT *)lParam)->lpCreateParams;
		object->window = hwnd;
		return object->windows_callback(hwnd, message, wParam, lParam);
		}
	else if ((object = (terminal *)GetWindowLongPtr(hwnd, GWLP_USERDATA)) != NULL)
		return object->windows_callback(hwnd, message, wParam, lParam);
	else
		return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	TERMINAL::WINDOWS_CALLBACK()
	----------------------------
*/
#ifdef _WIN32
	LRESULT terminal::windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	long terminal::windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	RECT client_size, window_size;

	switch(message)
		{
		/*
			Create Window
		*/
		case WM_CREATE:		
			/*
				Set the size of the canvas to 12 pixels * WIDTH
			*/
			GetClientRect(hwnd, &client_size);
			GetWindowRect(hwnd, &window_size);
			SetWindowPos(hwnd, HWND_TOP, window_size.top, window_size.left, WIDTH_IN_PIXELS + (WIDTH_IN_PIXELS - client_size.right), 2 * HEIGHT_IN_PIXELS + (2 * HEIGHT_IN_PIXELS - client_size.bottom), 0);
			return 0;

		/*
			Keyboard processing
		*/
		case WM_CHAR:
			/*
				User pressed a key so we shove it in a circular buffer
			*/
			*keyboard_buffer_write_pos++ = (char)wParam;
			if (keyboard_buffer_write_pos > keyboard_buffer + sizeof(keyboard_buffer))
				keyboard_buffer_write_pos = keyboard_buffer;

			return 0;

		/*
			Redraw the window
		*/
		case WM_PAINT:
			window_paint(hwnd);
			return 0;

		/*
			Close the window
		*/
		case WM_QUIT:
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		/*
			Cursor flashing
		*/
		case WM_TIMER:
			flash_state = !flash_state;
			RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
			return 0;

		}
	return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	TERMINAL::CREATE_WINDOW()
	-------------------------
*/
long terminal::create_window(const char *window_title, long mode)
{
#ifdef _WIN32
	HWND window;
	WNDCLASSEX windowClass;

	this->mode = mode;

	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_GLOBALCLASS;
	windowClass.lpfnWndProc = ::windows_callback;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = 0;
	windowClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_TERMINAL));
	windowClass.hCursor = NULL;
	windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = "ProteusTerminal";
	windowClass.hIconSm = NULL;

	RegisterClassEx(&windowClass);

	window = CreateWindowEx(NULL,			// extended style
		"ProteusTerminal",					// class name
		window_title,					// window name
		WS_EX_OVERLAPPEDWINDOW | WS_BORDER| WS_SYSMENU | ((mode & CREATE_ON_WRITE) ? 0 : WS_VISIBLE),
		120, 120,			// x/y coords
		WIDTH_IN_PIXELS,	// width
		2 * HEIGHT_IN_PIXELS,	// height
		NULL,				// handle to parent
		NULL,				// Menu
		0,					// application instance
		this);				// no extra parameter's

	hidden = (mode & CREATE_ON_WRITE) ? TRUE : FALSE;
	auto_nl = (mode & AUTO_NL) ? TRUE : FALSE;

	UpdateWindow(window);
	SetWindowLongPtr(window, GWLP_USERDATA, (LONG_PTR)this);

	if (!(mode & NO_CURSOR))
		SetTimer(window, 0, FLASH_INTERVAL, NULL);

	return 0;
#else
	return 0;
#endif
}

/*
	TERMINAL::PRINT_CHAR()
	----------------------
*/
void terminal::print_char(long pos_x, long pos_y, unsigned char ch)
{
long from, x, y, pos;
unsigned char *into;
unsigned char on, off, temp;

on = TEXT_COLOUR;
off = BACKGROUND_COLOUR;

if (flash_state && pos_x == cursor_x && pos_y == cursor_y)		// cursor flashing
	{
	temp = on;
	on = off;
	off = temp;
	}
if (ch & 0x80)		// inverse
	{
	temp = on;
	on = off;
	off = temp;
	ch = ch & 0x7F;		// turn high bit off
	}

if (ch < ' ')
	ch = ' ';

from = (long)(ch - ' ') * 10;		// 10 bytes per character

for (y = 0; y < 10; y++)
	{
	pos = font[from + y];
	into = canvas + (pos_x * 12 + pos_y * 10 * WIDTH_IN_PIXELS) + y * WIDTH_IN_PIXELS;

	for (x = 2; x < 8; x++)
		if (pos & (0x80 >> x))
			{
			*into++ = on;
			*into++ = on;
			}
		else
			{
			*into++ = off;
			*into++ = off;
			}
	}
}

/*
	TERMINAL::PAINT_TEXT()
	----------------------
*/
void terminal::paint_text(void)
{
long line, column;

for (line = 0; line < HEIGHT; line++)
	for (column = 0; column < WIDTH; column++)
		print_char(column, line, screen_buffer[(line * WIDTH) + column]);
}

/*
	TERMINAL::WINDOW_PAINT()
	------------------------
*/
#ifdef _WIN32
	void terminal::window_paint(HWND window)
#else
	void terminal::window_paint(long window)
#endif
{
#ifdef _WIN32
	PAINTSTRUCT paintStruct;
	HDC hDC;

	memset(canvas, BACKGROUND_COLOUR, WIDTH_IN_PIXELS * HEIGHT_IN_PIXELS);
	paint_text();

	hDC = BeginPaint(window, &paintStruct);
	StretchBlt(hDC, 0, 0, WIDTH_IN_PIXELS, 2 * HEIGHT_IN_PIXELS, bitmap, 0, 0, WIDTH_IN_PIXELS, HEIGHT_IN_PIXELS, SRCCOPY);
	EndPaint(window, &paintStruct);
#endif
}

/*
	TERMINAL::GETCH()
	-----------------
*/
int terminal::getch(void)
{
char answer;

if (kbhit())
	{
	answer = *keyboard_buffer_read_pos++;
	if (keyboard_buffer_read_pos > keyboard_buffer + sizeof(keyboard_buffer))
		keyboard_buffer_read_pos = keyboard_buffer;
	}
else
	return -1;		// EOF

return answer;
}

/*
	TERMINAL::PUTCH()
	-----------------
	ADM-31 Emulation
*/
void terminal::putch(unsigned char character)
{
/*
FILE *fp = fopen("a.out", "a+b");
fputc(character, fp);
fclose(fp);
*/

last_4_keys = (last_4_keys << 8) | character;

if ((last_4_keys & 0x0000FFFF) == 0x00001B28)		// ESC (		Normal Video
	inverse_video = FALSE;
else if ((last_4_keys & 0x0000FFFF) == 0x00001B29)	// ESC )		Reverse Video
	inverse_video = TRUE;
else if ((last_4_keys & 0x0000FFFF) == 0x00001B2A)	// ESC *		Clear Screen and Home Cursor
	{
	cursor_x = cursor_y = 0;
	memset(screen_buffer, ' ', sizeof(screen_buffer));
	}
else if ((last_4_keys & 0x0000FFFF) == 0x00001B51) // ESC Q		Insert space character (and shift to the right)
	{
	if (cursor_x < (WIDTH - 1))
		memmove(screen_buffer + cursor_y * WIDTH + cursor_x + 1, screen_buffer + cursor_y * WIDTH + cursor_x, WIDTH - cursor_x - 1);
	screen_buffer[cursor_y * WIDTH + cursor_x] = ' ';
	}
else if ((last_4_keys & 0x0000FFFF) == 0x00001B57) // ESC W		Delete Character
	{
	memmove(screen_buffer + cursor_y * WIDTH + cursor_x, screen_buffer + cursor_y * WIDTH + cursor_x + 1, WIDTH - cursor_x - 1);
	screen_buffer[cursor_y * WIDTH + (WIDTH - 1)] = ' ';
	}
else if ((last_4_keys & 0x0000FFFF) == 0x00001B45) // ESC E		Insert current line
	{
	cursor_x = 0;
	if (cursor_y < (HEIGHT - 1))
		memmove(screen_buffer + (cursor_y + 1) * WIDTH, screen_buffer + cursor_y * WIDTH, WIDTH * (HEIGHT - cursor_y - 1));
	memset(screen_buffer + cursor_y * WIDTH, ' ', WIDTH);
	}
else if ((last_4_keys & 0x0000FFFF) == 0x00001B52) // ESC R		Delete current line
	{
	cursor_x = 0;
	if (cursor_y < (HEIGHT - 1))
		memmove(screen_buffer + cursor_y * WIDTH, screen_buffer + (cursor_y + 1) * WIDTH, (HEIGHT - cursor_y - 1) * WIDTH);
	memset(screen_buffer + (HEIGHT - 1) * WIDTH, ' ', WIDTH);
	}
else if ((last_4_keys & 0x0000FFFF) == 0x00001B54) // ESC T		Clear to end of line
	memset(screen_buffer + cursor_y * WIDTH + cursor_x, ' ', WIDTH - cursor_x);
else if ((last_4_keys & 0x0000FFFF) == 0x00001B59) // ESC Y		Clear to end of screen
	memset(screen_buffer + cursor_y * WIDTH + cursor_x, ' ', sizeof(screen_buffer) - (cursor_y * WIDTH + cursor_x));
else if ((last_4_keys & 0x0000FFFF) == 0x00001B3D)	// ESC =
	{}			// nothing
else if ((last_4_keys & 0x00FFFF00) == 0x001B3D00)	// ESC = ?
	{}			// nothing
else if ((last_4_keys & 0xFFFF0000) == 0x1B3D0000)	// ESC = ? ?	Goto row col
	{
	cursor_y = ((last_4_keys >> 8) & 0xFF) - ' ';
	cursor_x = (last_4_keys & 0xFF) - ' ';
	}
else		// All else
	{
	switch (character)
		{
		case 7:				// ^G Bell
			break;
		case 8:				// ^H Cursor left
			cursor_x--;
			break;
		case 9:				// ^I Tab
			++cursor_x;
			while ((cursor_x % TABSTOPS) != 0)
				cursor_x++;
			break;
		case 10:			// ^J Linefeed
			cursor_y++;
			break;
		case 11:			// ^K Cursor up
			cursor_y--;
			break;
		case 12:			// ^L Cursor right
			cursor_x++;
			if (cursor_x > WIDTH)
				cursor_x = 0;
			break;
		case 13:			// ^M Return
			cursor_x = 0;
			break;
		case 26:			// Clear screen and home cursor
			cursor_x = cursor_y = 0;
			memset(screen_buffer, ' ', sizeof(screen_buffer));
			break;
		case 30:			// ^^ Home cursor
			cursor_x = cursor_y = 0;
			break;
		default:
			if (character > 27 && character < 128)
				{
				if (inverse_video)
					screen_buffer[cursor_y * WIDTH + cursor_x] = 0x80 | character;
				else
					screen_buffer[cursor_y * WIDTH + cursor_x] = character;
				cursor_x++;
				}
			break;
		}
	}

if (cursor_x < 0)
	cursor_x = 0;
else if (cursor_x >= WIDTH)			// Line wrap
	{
	if (auto_nl)
		{
		/*
			In Auto-nl mode we scroll at the edge of the screen.  Otherwise we stay at collumn 80.
		*/
		cursor_x = 0;
		cursor_y++;
		}
	else
		cursor_x = WIDTH - 1;
	}

if (cursor_y < 0)
	cursor_y = 0;
else if (cursor_y >= HEIGHT)			// Scroll
	{
	memmove(screen_buffer, screen_buffer + WIDTH, sizeof(screen_buffer) - WIDTH);
	memset(screen_buffer + (HEIGHT - 1) * WIDTH, ' ', WIDTH);
	cursor_y = HEIGHT - 1;
	}

#ifdef _WIN32
	if (hidden)
		{
		hidden = FALSE;
		ShowWindow(window, SW_SHOW);
		}

	RedrawWindow(window, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
#endif
}
