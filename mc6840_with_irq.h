/*
	MC6840_WITH_IRQ.H
	-----------------
*/
#ifndef MC6840_WITH_IRQ_H_
#define MC6840_WITH_IRQ_H_

#include "mc6840.h"
#include "poly.h"

/*
	class MC6840_WITH_IRQ
	---------------------
*/
class mc6840_with_irq : public mc6840
{
private:
	poly *cpu;

public:
	mc6840_with_irq(poly *cpu) { this->cpu = cpu; }
	virtual ~mc6840_with_irq() {}

	virtual void irq(void)  { cpu->timer_irq(); }
	virtual void d_irq(void) { cpu->timer_d_irq(); }
} ;


#endif /* MC6840_WITH_IRQ_H_ */
