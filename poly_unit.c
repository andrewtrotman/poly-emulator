/*
	POLY_UNIT.C
	-----------
*/
#ifdef _WIN32
	#include <windows.h>
	#include "resources/resource.h"
#endif
#include "poly_unit.h"
#include "saa5050.h"
#include "dib.h"
#include "proteus_unit.h"

#define VK_SCROLLLOCK (0x91)

/*
	POLY_UNIT::POLY_UNIT()
	----------------------
*/
#ifdef _WIN32
	poly_unit::poly_unit(HINSTANCE hInstance, proteus *server, poly **clients)
#else
	poly_unit::poly_unit(long hInstance, proteus *server, poly **clients)
#endif
{
#ifdef _WIN32
	BITMAP bitmap;
#endif

this->hInstance = hInstance;
this->server = server; 
this->clients = clients;

client = new poly(hInstance); 
client->reset(); 
shift = FALSE; 
set_keyboard_scan_codes(1); 
text_flash_state = 0;

#ifdef _WIN32
	bmp_about = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_POLY_PEOPLE));
	GetObject(bmp_about, sizeof(bitmap), &bitmap);
	bmp_about_width = bitmap.bmWidth;
	bmp_about_height = bitmap.bmHeight;
#endif
}

/*
	ABOUTDLGPROC()
	--------------
*/
#ifdef _WIN32
	static INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static bool AboutDlgProc(long hDlg, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	poly_unit *object = NULL;

	if (message == WM_INITDIALOG)
		SetWindowLongPtr(hDlg, GWLP_USERDATA, (LONG_PTR)lParam);

	if ((object = (poly_unit *)GetWindowLongPtr(hDlg, GWLP_USERDATA)) != NULL)
		return object->about_box(hDlg, message, wParam, lParam);
	else
		return FALSE;
#else
	return FALSE;
#endif
}

/*
	POLY_UNIT::ABOUT_BOX()
	----------------------
*/
#ifdef _WIN32
	INT_PTR CALLBACK poly_unit::about_box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
#else
	long poly_unit::about_box(long hDlg, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	HWND button;
	HDC hdcMem;
	HGDIOBJ hbmOld;
	RECT client_size, window_size, button_size, button_window_size;

	switch (message)
		{
		case WM_ERASEBKGND:
			hdcMem = CreateCompatibleDC((HDC)wParam);
			hbmOld = SelectObject(hdcMem, bmp_about);
			BitBlt((HDC)wParam, 0, 0, bmp_about_width, bmp_about_height, hdcMem, 0, 0, SRCCOPY);
			StretchBlt((HDC)wParam, 0, bmp_about_height - 1, bmp_about_width, bmp_about_height, hdcMem, 0, 0, 1, 1, SRCCOPY);
			DeleteDC(hdcMem);
			return TRUE;
		case WM_INITDIALOG:
			/*
				Set the size of the dialog box
			*/
			GetClientRect(hDlg, &client_size);
			GetWindowRect(hDlg, &window_size);
			button = GetDlgItem(hDlg, IDOK);
			GetClientRect(button, &button_size);
			SetWindowPos(hDlg, HWND_TOP, window_size.left, window_size.top, bmp_about_width + (window_size.right - window_size.left - client_size.right), bmp_about_height + (window_size.bottom  - window_size.top - client_size.bottom) + button_size.bottom, 0);

			/*
				Now move the button
			*/
			GetClientRect(hDlg, &client_size);			// This has change as a consequence of the SetWindowPos() call above
			GetWindowRect(button, &button_window_size);
			MoveWindow(button, (client_size.right - (button_window_size.right - button_window_size.left)) / 2, client_size.bottom - button_size.bottom - (window_size.bottom - button_window_size.bottom) / 2, button_size.right, button_size.bottom, TRUE);
			return TRUE;
		case WM_CTLCOLORDLG: 
			return (BOOL)((HBRUSH)GetStockObject(WHITE_BRUSH) != NULL);
		case WM_COMMAND:
			EndDialog(hDlg, 0);			// OK button
			return TRUE;
		}
	return FALSE;
#else
	return FALSE;
#endif
}

/*
	POLY_UNIT::MAKE_CANVAS()
	------------------------
*/
void poly_unit::make_canvas(void)
{
#ifdef _WIN32
	HGDIOBJ bmp;
	BITMAPINFO256 info;

	info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	info.bmiHeader.biWidth = WIDTH_IN_PIXELS;
	info.bmiHeader.biHeight = -HEIGHT_IN_PIXELS;			// -ve for a top-down DIB
	info.bmiHeader.biPlanes = 1;
	info.bmiHeader.biBitCount = 8;
	info.bmiHeader.biCompression = BI_RGB;		// not compressed
	info.bmiHeader.biSizeImage = 0;
	info.bmiHeader.biXPelsPerMeter = 0;
	info.bmiHeader.biYPelsPerMeter = 0;
	info.bmiHeader.biClrUsed = 0;
	info.bmiHeader.biClrImportant = 0;
	memset(info.bmiColors, 0, sizeof(info.bmiColors));
	memcpy(info.bmiColors, pallette, sizeof(pallette));		// copy the Poly's colour pallette
	bitmap = CreateCompatibleDC(NULL);
	bmp = CreateDIBSection(bitmap, (BITMAPINFO *)&info, DIB_RGB_COLORS, (void **)&canvas, NULL, 0);
	SelectObject(bitmap, bmp);
#endif
}

/*
	POLY_UNIT::SET_KEYBOARD_SCAN_CODES()
	------------------------------------
*/
void poly_unit::set_keyboard_scan_codes(long poly_type)
{
if (poly_type == 1)
	{
	poly_key_line_insert = 0x10;
	poly_key_char_insert = 0x01;
	poly_key_line_del = 0x04;
	poly_key_char_del = 0x02;
	poly_key_calc = 0x5E;
	poly_key_help = 0x7E;
	poly_key_exit = 0x7B;
	poly_key_back = 0x5B;
	poly_key_repeat = 0x7D;
	poly_key_next = 0x5D;
	poly_key_pause = 0x1B;
	poly_key_left = 0x08;
	poly_key_right = 0x09;
	poly_key_up = 0x0B;
	poly_key_down = 0x0A;
	poly_key_u0 = 0x5F;
	poly_key_u1 = 0x14;
	poly_key_u2 = 0x15;
	poly_key_u3 = 0x16;
	poly_key_u4 = 0x17;
	poly_key_u5 = 0x19;
	poly_key_u6 = 0x1A;
	poly_key_u7 = 0x7F;
	poly_key_u8 = 0x1D;
	poly_key_u9 = 0x06;
	poly_key_at = 0x7C;
	poly_key_bar = 0x40;
	poly_key_exp = 0x60;
	poly_key_pound = 0x5C;

	// the following are not on the Poly 1 keyboard!
	poly_key_shift_pause = 0x00;
	poly_key_keypad_dot = 0x00;
	}
else if (poly_type == 2)
	{
	poly_key_line_insert = 0x7B;
	poly_key_char_insert = 0x5B;
	poly_key_line_del = 0x5E;
	poly_key_char_del = 0x7E;
	poly_key_calc = 0x12;
	poly_key_help = 0x1D;
	poly_key_exit = 0x5C;
	poly_key_back = 0x7C;
	poly_key_repeat = 0x7D;
	poly_key_next = 0x5D;
	poly_key_pause = 0x0F;
	poly_key_left = 0x1F;
	poly_key_right = 0x11;
	poly_key_up = 0x08;
	poly_key_down = 0x0B;
	poly_key_u0 = 0x16;
	poly_key_u1 = 0x15;
	poly_key_u2 = 0x18;
	poly_key_u3 = 0x01;
	poly_key_u4 = 0x7F;
	poly_key_u5 = 0x19;
	poly_key_u6 = 0x06;
	poly_key_u7 = 0x1A;
	poly_key_u8 = 0x0A;
	poly_key_u9 = 0x17;
	poly_key_at = 0x09;
	poly_key_bar = 0x40;
	poly_key_exp = 0x60;
	poly_key_pound = 0x13;

	// Poly 2 keyboard only
	poly_key_shift_pause = 0x0E;
	poly_key_keypad_dot = 0x1E;
	}
}

/*
	POLY_UNIT::TRANSLATE_KEY()
	--------------------------
*/
#ifdef _WIN32
	unsigned char poly_unit::translate_key(WPARAM wParam)
#else
	unsigned char poly_unit::translate_key(long wParam)
#endif
{
#ifdef _WIN32
	unsigned char key_states[0x100];
	WORD key;

	switch(wParam)
		{
		/*
			On the Poly 1 from which the ROMS were extracted
			there is no scan code for Keypad-Dot(26), Line-Del(25), or Shift-Pause(27)
		*/
		case VK_SHIFT:
			shift = !shift;
			return 0;
		case VK_INSERT:
			return shift ? poly_key_line_insert : poly_key_char_insert;
		case VK_DELETE:
			return shift ? poly_key_line_del : poly_key_char_del;
		case VK_NEXT:			// PageDown
			return poly_key_next;
		case VK_PRIOR:			// PageUp
			return poly_key_back;
		case VK_CANCEL:			// Ctrl-Break
			return poly_key_exit;
		case VK_PAUSE:									// Pause
			if (shift && poly_key_shift_pause != 0)
				return poly_key_shift_pause; 			// only on Poly 2
			else
				return poly_key_pause;
		case VK_F11:
			return poly_key_help;
		case VK_F12:
			return poly_key_calc;
		case VK_ESCAPE:
			return poly_key_repeat;
		case VK_BACK:		// backspace is a left-arrow
		case VK_LEFT:
			return poly_key_left;
		case VK_RIGHT:
			return poly_key_right;
		case VK_UP:
			return poly_key_up;
		case VK_DOWN:
			return poly_key_down;
		case VK_NUMPAD0:
		case VK_F10:
			return poly_key_u0;
		case VK_NUMPAD1:
		case VK_F1:
			return poly_key_u1;
		case VK_NUMPAD2:
		case VK_F2:
			return poly_key_u2;
		case VK_NUMPAD3:
		case VK_F3:
			return poly_key_u3;
		case VK_NUMPAD4:
		case VK_F4:
			return poly_key_u4;
		case VK_NUMPAD5:
		case VK_F5:
			return poly_key_u5;
		case VK_NUMPAD6:
		case VK_F6:
			return poly_key_u6;
		case VK_NUMPAD7:
		case VK_F7:
			return poly_key_u7;
		case VK_NUMPAD8:
		case VK_F8:
			return poly_key_u8;
		case VK_NUMPAD9:
		case VK_F9:
			return poly_key_u9;
		case VK_DECIMAL:								// only on Poly 2
			return poly_key_keypad_dot;
		default:
			GetKeyboardState(key_states);
			if (ToAscii(wParam, MapVirtualKey(wParam, 0), key_states, &key, 0) == 1)
				{
				switch (key & 0xFF)
					{
					case '@':
						return poly_key_at;
					case '|':
						return poly_key_bar;		// |
					case '^':
						return poly_key_exp;
					case '�':
					case '`':
						return poly_key_pound;		// Pound
					default:
						return key & 0xFF;
					}
				}
			return 0;
			}
#else
	return 0;
#endif
}

/*
	POLY_UNIT::SET_ROM_VERSION()
	----------------------------
*/
#ifdef _WIN32
	void poly_unit::set_rom_version(WORD option)
#else
	void poly_unit::set_rom_version(long option)
#endif
{
	#ifdef _WIN32
	HMENU menubar;
	menubar = GetMenu(window);

	CheckMenuItem(menubar, ID_ROMS_BASIC_23, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(menubar, ID_ROMS_BASIC_30, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(menubar, ID_ROMS_BASIC_31, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(menubar, ID_ROMS_BASIC_34, MF_BYCOMMAND | MF_UNCHECKED);

	switch (option)
		{
		case ID_ROMS_BASIC_23:
			client->set_rom_version(23);
			set_keyboard_scan_codes(1);		// be a Poly 1
			client->reset();
			EnableMenuItem(menubar, 1, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);	// disable the disk menu
			break;
		case ID_ROMS_BASIC_30:
			client->set_rom_version(30);
			set_keyboard_scan_codes(2);		// be a Poly 2
			client->reset();
			EnableMenuItem(menubar, 1, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);	// disable the disk menu
			break;
		case ID_ROMS_BASIC_31:
			client->set_rom_version(31);
			set_keyboard_scan_codes(2);		// be a Poly 2
			client->reset();
			EnableMenuItem(menubar, 1, MF_BYPOSITION | MF_DISABLED | MF_GRAYED);	// disable the disk menu
			break;
		case ID_ROMS_BASIC_34:
			client->set_rom_version(34);
			set_keyboard_scan_codes(2);		// be a Poly 2
			client->reset();
			EnableMenuItem(menubar, 1, MF_BYPOSITION | MF_ENABLED);		// enable the disk menu
			break;
		}

	CheckMenuItem(menubar, option, MF_BYCOMMAND | MF_CHECKED);
	DrawMenuBar(window);
#endif
}

/*
	POLY_UNIT::MENU()
	-----------------
*/
#ifdef _WIN32
	void poly_unit::menu(WORD clicked)
#else
	void poly_unit::menu(long clicked)
#endif
{
#ifdef _WIN32
	switch (clicked)
		{
		/*
			FILE MENU
		*/
		case ID_FILE_RESET:
			client->reset();
			break;

		case ID_FILE_WARM_RESET:
			client->reset();
			break;

		case ID_FILE_EXIT:
			PostQuitMessage(0);
			break;

		/*
			DRIVE MENU
		*/
		case ID_DRIVE_0:
		case ID_DRIVE_1:
		case ID_DRIVE_2:
		case ID_DRIVE_3:
			proteus_unit::load_disk(client, window, clicked);
			break;

		/*
			ROMS MENU
		*/
		case ID_ROMS_BASIC_23:
		case ID_ROMS_BASIC_30:
		case ID_ROMS_BASIC_31:
		case ID_ROMS_BASIC_34:
			set_rom_version(clicked);
			break;
		/*
			KEYS MENU
		*/
		case ID_KEYS_BACK:
			client->putch(poly_key_back, 1);
			client->putch(poly_key_back, 0);
			break;

		case ID_KEYS_REPEAT:
			client->putch(poly_key_repeat, 1);
			client->putch(poly_key_repeat, 0);
			break;

		case ID_KEYS_NEXT:
			client->putch(poly_key_next, 1);
			client->putch(poly_key_next, 0);
			break;

		case ID_KEYS_EXIT:
			client->putch(poly_key_exit, 1);
			client->putch(poly_key_exit, 0);
			break;

		case ID_KEYS_CALC:
			client->putch(poly_key_calc, 1);
			client->putch(poly_key_calc, 0);
			break;

		case ID_KEYS_HELP:
			client->putch(poly_key_help, 1);
			client->putch(poly_key_help, 0);
			break;

		case ID_KEYS_CHARINSERT:
			client->putch(poly_key_char_insert, 1);
			client->putch(poly_key_char_insert, 0);
			break;

		case ID_KEYS_CHARDELETE:
			client->putch(poly_key_char_del, 1);
			client->putch(poly_key_char_del, 0);
			break;

		case ID_KEYS_LINEINSERT:
			client->putch(poly_key_line_insert, 1);
			client->putch(poly_key_line_insert, 0);
			break;

		case ID_KEYS_LINEDELETE:
			client->putch(poly_key_line_del, 1);
			client->putch(poly_key_line_del, 0);
			break;

		case ID_KEYS_UP:
			client->putch(poly_key_up, 1);
			client->putch(poly_key_up, 0);
			break;

		case ID_KEYS_DOWN:
			client->putch(poly_key_down, 1);
			client->putch(poly_key_down, 0);
			break;

		case ID_KEYS_LEFT:
			client->putch(poly_key_left, 1);
			client->putch(poly_key_left, 0);
			break;

		case ID_KEYS_RIGHT:
			client->putch(poly_key_right, 1);
			client->putch(poly_key_right, 0);
			break;

		case ID_KEYS_U0:
			client->putch(poly_key_u0, 1);
			client->putch(poly_key_u0, 0);
			break;

		case ID_KEYS_U1:
			client->putch(poly_key_u1, 1);
			client->putch(poly_key_u1, 0);
			break;

		case ID_KEYS_U2:
			client->putch(poly_key_u2, 1);
			client->putch(poly_key_u2, 0);
			break;

		case ID_KEYS_U3:
			client->putch(poly_key_u3, 1);
			client->putch(poly_key_u3, 0);
			break;

		case ID_KEYS_U4:
			client->putch(poly_key_u4, 1);
			client->putch(poly_key_u4, 0);
			break;

		case ID_KEYS_U5:
			client->putch(poly_key_u5, 1);
			client->putch(poly_key_u5, 0);
			break;

		case ID_KEYS_U6:
			client->putch(poly_key_u6, 1);
			client->putch(poly_key_u6, 0);
			break;

		case ID_KEYS_U7:
			client->putch(poly_key_u7, 1);
			client->putch(poly_key_u7, 0);
			break;

		case ID_KEYS_U8:
			client->putch(poly_key_u8, 1);
			client->putch(poly_key_u8, 0);
			break;

		case ID_KEYS_U9:
			client->putch(poly_key_u9, 1);
			client->putch(poly_key_u9, 0);
			break;

		case ID_KEYS_PAUSE:
			client->putch(poly_key_pause, 1);
			client->putch(poly_key_pause, 0);
			break;

		case ID_KEYS_SHIFT_PAUSE:
			if (poly_key_shift_pause != 0)		// only on Poly 2
				{
				client->putch(poly_key_shift_pause, 1);
				client->putch(poly_key_shift_pause, 0);
				}
			break;

		case ID_KEYS_DOT:
			if (poly_key_keypad_dot != 0)		// only on Poly 2
				{
				client->putch(poly_key_keypad_dot, 1);
				client->putch(poly_key_keypad_dot, 0);
				}
			break;
		/*
			HELP MENU
		*/
		case ID_HELP_ABOUT_POLY:
				DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_ABOUT_POLY), window, AboutDlgProc, (LPARAM)this);
			break;
		}
#endif
}

/*
	POLY_UNIT::WINDOWS_CALLBACK()
	-----------------------------
*/
#ifdef _WIN32
	LRESULT poly_unit::windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	long poly_unit::windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	long long cycles_spent, cycles_to_spend, cycles_per_key_event;
	HDC hDC; 
	RECT client_size, window_size;
	PAINTSTRUCT paintStruct;
	unsigned char poly_key;
	unsigned long display_context;
	poly **current_client;

	switch(message)
		{
		case WM_CREATE:
			GetClientRect(hwnd, &client_size);
			GetWindowRect(hwnd, &window_size);
			SetWindowPos(hwnd, HWND_TOP, window_size.top, window_size.left, WIDTH_IN_PIXELS + (WIDTH_IN_PIXELS - client_size.right), 2 * HEIGHT_IN_PIXELS + (2 * HEIGHT_IN_PIXELS - client_size.bottom), 0);
			make_canvas();
			set_rom_version(ID_ROMS_BASIC_23);
			return 0;

		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
			if ((poly_key = translate_key(wParam)) != 0)
				client->putch(poly_key, 1);
			return 0;

		case WM_SYSKEYUP:
		case WM_KEYUP:
			if ((poly_key = translate_key(wParam)) != 0)
				client->putch(poly_key, 0);

		case WM_PAINT:
			memset(canvas, PALLETTE_BACKGROUND_BASE + ((client->pia->in_b() >> 4) & 0x07), WIDTH_IN_PIXELS * HEIGHT_IN_PIXELS);
			display_context = client->pia->in_b() << 8 | client->pia->in_a();
			
			if ((display_context & 0x0210) == 0x0200)			// screen 4 (no 480)
				client->graphics_screen->render_page(canvas, client->memory + 0x8000, (client->pia->in_b() & 0x0C) >> 2);

			if ((display_context & 0x0100) == 0x0100)			// screen 3
				client->screen_3->paint_text_page(canvas, text_flash_state);

			if ((display_context & 0x0030) == 0x0030)			// screen 5
				client->graphics_screen->render_combined_pages(canvas, client->memory + 0x4000, client->memory + 0x8000);

			if ((display_context & 0x0030) == 0x0020)			// screen 2
				client->graphics_screen->render_page(canvas, client->memory + 0x4000, (client->pia->in_a() & 0x06) >> 1);

			if ((display_context & 0x0270) == 0x0260)			// screens 2 and 4 colour mixed (the routine below only draws mixed pixels)
				client->graphics_screen->render_mixed_pages(canvas, client->memory + 0x4000, client->memory + 0x8000);

			if ((display_context & 0x0008) == 0x0008)			// screen 1
				client->screen_1->paint_text_page(canvas, text_flash_state);

			hDC = BeginPaint(hwnd, &paintStruct);
			StretchBlt(hDC, 0, 0, WIDTH_IN_PIXELS, 2 * HEIGHT_IN_PIXELS, bitmap, 0, 0, WIDTH_IN_PIXELS, HEIGHT_IN_PIXELS, SRCCOPY);
			EndPaint(hwnd, &paintStruct);
			return 0;

		case WM_TIMER:
			if (wParam == TIMER_TEXT_FLASH)
				{
				text_flash_state = !text_flash_state;
				RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
				}
			else if (wParam == TIMER_DISPLAY_REFRESH)
				RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
			else if (wParam == TIMER_CPU_TICK)
				{
				/*
					Run for a 10th of an emulated second (clock speed / 10)
				*/
				cycles_spent = client->cycles; 
				cycles_to_spend = client->cpu_frequency / 10;
				cycles_per_key_event = cycles_to_spend / 100;
				while (client->cycles < cycles_spent + cycles_to_spend)
					{
					if (client->cycles % cycles_per_key_event == 0)
						{
						client->process_next_keyboard_event();
						if (clients != NULL)
							for (current_client = clients; *current_client != NULL; current_client++)
								(*current_client)->process_next_keyboard_event();
						}
					client->step();
					if (clients != NULL)
						for (current_client = clients; *current_client != NULL; current_client++)
							(*current_client)->step();
					if (server != NULL)
						server->step();
					}

				/*
					Now refresh the screen
				*/
				if (client->changes != 0)
					{
					RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_INTERNALPAINT);
					client->changes = 0;
					}
				}
			return 0;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDCANCEL)
				{
				/*
					Look, see, I didn't write windows!  the ESCAPE key is encoded as an IDCANCEL command!
				*/
				if ((poly_key = translate_key(VK_ESCAPE)) != 0)
					client->putch(poly_key, 1);
				}
			else
				menu(LOWORD(wParam));
			return 0;
		}
	return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	WINDOWS_CALLBACK()
	------------------
*/
#ifdef _WIN32
	static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
#else
	static long windows_callback(long hwnd, long message, long wParam, long lParam)
#endif
{
#ifdef _WIN32
	poly_unit *object = NULL;

	if (message == WM_CREATE)
		{
		object = (poly_unit *)((CREATESTRUCT *)lParam)->lpCreateParams;
		object->window = hwnd;
		return object->windows_callback(hwnd, message, wParam, lParam);
		}
	else if ((object = (poly_unit *)GetWindowLongPtr(hwnd, GWLP_USERDATA)) != NULL)
		return object->windows_callback(hwnd, message, wParam, lParam);
	else
		return (DefWindowProc(hwnd,message,wParam,lParam));
#else
	return 0;
#endif
}

/*
	POLY_UNIT::CREATE_WINDOW()
	--------------------------
*/
long poly_unit::create_window(const char *window_title, long mode)
{
#ifdef _WIN32
	HWND window;
	WNDCLASSEX windowClass;

	this->hInstance = hInstance;
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_GLOBALCLASS;
	windowClass.lpfnWndProc = ::windows_callback;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = 0;
	windowClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_POLY));
	windowClass.hCursor = NULL;
	windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = "PolyTerminal";
	windowClass.hIconSm = NULL;

	RegisterClassEx(&windowClass);

	window = CreateWindowEx(NULL,			// extended style
		"PolyTerminal",					// class name
		window_title,					// window name
		WS_EX_OVERLAPPEDWINDOW | WS_BORDER| WS_SYSMENU | WS_VISIBLE,
		120, 120,			// x/y coords
		WIDTH_IN_PIXELS,	// width
		2 * HEIGHT_IN_PIXELS,	// height
		NULL,				// handle to parent
		LoadMenu(0, MAKEINTRESOURCE(IDR_MENU_POLY)),				// Menu
		0,					// application instance
		this);				// no extra parameter's

	SetWindowLongPtr(window, GWLP_USERDATA, (LONG_PTR)this);

	UpdateWindow(window);

	SetTimer(window, TIMER_TEXT_FLASH, 500, NULL);
	SetTimer(window, TIMER_DISPLAY_REFRESH, 40, NULL);

	if (mode == ACTIVE)
		SetTimer(window, TIMER_CPU_TICK, 100, NULL);			// run for a 10th of a second every 100 miliseconds

	return 0;
#else
	return 0;
#endif
}

/*
	POLY_UNIT::CONNECT_TO()
	-----------------------
*/
poly *poly_unit::connect_to(proteus *server)
{
server->network->set_outstream(client->network);
client->network->set_outstream(server->network);

return client;		// the next unit should connect to the "client" unit
}

/*
	POLY_UNIT::CONNECT_TO()
	-----------------------
*/
poly *poly_unit::connect_to(poly *server)
{
server->network->set_upstream(client->network);
client->network->set_outoutstream(server->network);

return client;		// the next unit should connect to the "client" unit
}
