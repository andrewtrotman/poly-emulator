/*
	POLY.C
	------
*/
#ifdef _WIN32
	#include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "poly.h"
#include "poly_roms.h"
#include "mc6821_with_keyboard.h"
#include "mc6840_with_speaker.h"

#ifndef FALSE
	#define FALSE 0
#endif
#ifndef TRUE
	#define TRUE (!FALSE)
#endif

/*
	POLY::POLY()
	------------
*/
#ifdef _WIN32
	poly::poly(HINSTANCE hInstance) : proteus(hInstance, proteus::PASSIVE_TERMINAL)
#else
	poly::poly(long hInstance) : proteus(hInstance, proteus::PASSIVE_TERMINAL)
#endif
{
this->hInstance = hInstance;
screen_1 = NULL;
screen_3 = NULL;
pia2 = NULL;
graphics_screen = NULL;
delete timer;
timer = new mc6840_with_speaker(this);
set_rom_version(23);		// start with verison 2.3 of the ROMs
reset();
}

/*
	POLY::~POLY()
	-------------
*/
poly::~poly()
{
delete screen_1;
delete screen_3;
delete pia2;
delete graphics_screen;
}

/*
	POLY::SET_ROM_VERSION()
	-----------------------
*/
long poly::set_rom_version(long version)
{
if (version == 23 || version == 30 || version == 31 || version == 34)
	return rom_version = version;

/*
	Unknown ROM version so use version 2.3
*/
rom_version = 23;
return 0;
}

/*
	POLY::GET_ROM()
	---------------
*/
void poly::get_rom(void)
{
switch (rom_version)
	{
	case 23:
		memcpy(memory + 0xC000, poly_basic_23_1, 0x1000);
		memcpy(memory + 0xD000, poly_basic_23_2, 0x1000);
		memcpy(memory + 0xE000, poly_basic_23_3, 0x1000);
		memcpy(memory + 0xF000, poly_basic_23_4, 0x1000);
		poly_bios_23[0xFF1]++;
		memcpy(bios, poly_bios_23, 0x1000);
		break;
	case 30:
		memcpy(memory + 0xC000, poly_basic_30_1, 0x1000);
		memcpy(memory + 0xD000, poly_basic_30_2, 0x1000);
		memcpy(memory + 0xE000, poly_basic_30_3, 0x1000);
		memcpy(memory + 0xF000, poly_basic_30_4, 0x1000);
		memcpy(bios, poly_bios_30, 0x1000);
		break;
	case 31:
		memcpy(memory + 0xC000, poly_basic_31_1, 0x1000);
		memcpy(memory + 0xD000, poly_basic_31_2, 0x1000);
		memcpy(memory + 0xE000, poly_basic_31_3, 0x1000);
		memcpy(memory + 0xF000, poly_basic_31_4, 0x1000);
		memcpy(bios, poly_bios_31, 0x1000);
		break;
	case 34:
		memcpy(memory + 0xC000, poly_basic_34_1, 0x1000);
		memcpy(memory + 0xD000, poly_basic_34_2, 0x1000);
		memcpy(memory + 0xE000, poly_basic_34_3, 0x1000);
		memcpy(memory + 0xF000, poly_basic_34_4, 0x1000);
		memcpy(bios, poly_bios_34, 0x1000);
		break;
	}
}

/*
	POLY::RESET()
	-------------
*/
void poly::reset(void)
{
prot = TRUE;
cycles_before_leaving_prot = -1;
changes = 0;

memset(memory, 0, sizeof(memory));
get_rom();

delete screen_1;
screen_1 = new saa5050(video_memory);

delete screen_3;
screen_3 = new saa5050(video_memory + 1024);		// each screen is 1KB

delete pia2;
pia2 = new mc6821_with_keyboard(this);

delete graphics_screen;
graphics_screen = new graphics;

timer->reset();
keyboard_buffer_read_pos = keyboard_buffer_write_pos = keyboard_buffer;

memset(dat, 0, sizeof(dat));
dat_bank = 0;
baud_rate_register = 0;

timer_irqpend = keyboard_irqpend = network_irqpend = 0;
proteus::reset();
drive_select = 0;
}

/*
	POLY::PUTCH()
	-------------
*/
void poly::putch(unsigned char ascii, unsigned char down)
{
keyboard_buffer_write_pos->ascii = ascii | (down << 7);
keyboard_buffer_write_pos->cb1 = down;
keyboard_buffer_write_pos->cb2 = 0;

keyboard_buffer_write_pos++;
if (keyboard_buffer_write_pos >= keyboard_buffer + (sizeof(keyboard_buffer) / sizeof(*keyboard_buffer)))
	keyboard_buffer_write_pos = keyboard_buffer;
}

/*
	POLY::PROCESS_NEXT_KEYBOARD_EVENT()
	-----------------------------------
*/
void poly::process_next_keyboard_event(void)
{
if (keyboard_buffer_read_pos != keyboard_buffer_write_pos)
	{
	pia2->arrived_b(keyboard_buffer_read_pos->ascii, keyboard_buffer_read_pos->cb1, keyboard_buffer_read_pos->cb2);

	keyboard_buffer_read_pos++;
	if (keyboard_buffer_read_pos >= keyboard_buffer + (sizeof(keyboard_buffer) / sizeof(*keyboard_buffer)))
		keyboard_buffer_read_pos = keyboard_buffer;
	}
}

/*
	POLY::NETWORK_IRQ()
	-------------------
*/
void poly::network_irq(void)
{ 
extern long mc6854_channel_logging;
long was_logging = mc6854_channel_logging;
unsigned short relay_value;

if (!network->discontinue)
	network_irqpend = 1;

if (network->loop)
	{
	if (!network->rts)
		{
		/*
			Send back to the server
		*/
		mc6854_channel_logging = FALSE;
		relay_value = network->channel_in->last_write();
		network->channel_out->resend(relay_value);
		mc6854_channel_logging = was_logging;
		}
	else if (network->channel_up != NULL)
		{
		/*
			Send upstream to the next Poly
		*/
		mc6854_channel_logging  = FALSE;
		relay_value = network->channel_in->last_write();
		network->channel_up->resend(relay_value);
		mc6854_channel_logging = was_logging;
		}
	}

if (network->discontinue)
	{
	mc6854_channel_logging  = FALSE;
	relay_value = network->channel_in->last_write();
	network->channel_in->recieve();
	if (relay_value & mc6854::flag_eot)
		network->discontinue = 0;
	mc6854_channel_logging = was_logging;
	}
}

/*
	POLY::PHYSICAL()
	----------------
	Translate a logical address into a physical address
	dat_bank selects which of the 2 DAT banks we use
	the top 3 bits of the address select the DAT register
	the dat register forms the top 4 address bits
	the bottom 13 bits of the address are the offset
	remember... the DAT is in 1s compliment

	[DAT][ADDRESS] = D DDDA AAAA AAAA AAAA
	where DAT = [B][ADDRESS] B AAA
*/
unsigned long poly::physical(unsigned short address)
{
return (((~dat[(dat_bank << 3) | (address >> 13)]) & 0x0F) << 13) | (address & 0x1FFF);
}


/*
	POLY::READ()
	------------
	PROTECTED MODE MEMORY MAP
	-------------------------
	0000-DFFF RAM						user-mode decoded RAM (see below)
	E000-E003 PIA   (MC6821)			Video Controller
	E004-E005 ACIA  (MC6850)			Serial Port
	E006      Baud Rate Register		Baud Rate Controller
	E00C-E00F PIA   (MC6821)			Keyboard
	E014      Drive Register			** undocumented **
	E018-E01B FDC   (WD1771)			** undocumented **
	E020-E027 PTM   (MC6840)			Real Time Clock
	E030-E036 ADLC  (MC6854)			Networking
	E040      Protect Register			Flip out of protected mode
	E050-E05F MMU DAT					Memory management unit (Dynamic Address Translation (DAT))
	E060      MMU Page 1				Select page 1 of the MMU
	E070      MMU Page 2				Select page 2 of the MMU
	E800-EBBF Teletext 1 screen			Text screen
	EBC0-EBFF RAM						System data
	EC00-EFBF Teletext 2 screen			Text screen
	EFC0-EFFF RAM						System data
	F000-FFFF ROM						Boot ROM


	USER MODE MEMORY MAP
	--------------------
	DAT	Physical 	Name
	0	0000		RAM 1	(16K RAM)
	1	2000		RAM 1
	2	4000		RAM 2	(16K RAM)		Graphics page 2 (0x4000-0x6000)
	3	6000		RAM 2
	4	8000		RAM 3	(16K RAM)		Graphics page 4 (0x8000-0xA000)
	5	A000		RAM 3
	6	C000		BASIC 	(8K ROM 1,2)
	7	E000		BASIC 	(8K ROM 3,4)
	8	10000		RAM 4	(64K RAM)
	9	12000		RAM 4
	A	14000		RAM 4
	B	16000		RAM 4
	C	18000		RAM 4
	D	1A000		RAM 4
	E	1C000		RAM 4
	F	1E000		RAM 4
*/
byte poly::read(word address)
{
byte answer;

if (!prot)
	answer = memory[physical(address)];			// User mode addressing
else
	{
	switch (address)
		{
		/*
			Screen controller
			MC6821 PIA (Parallel Controller) at E000-E003
		*/
		case 0xE000:
		case 0xE001:
		case 0xE002:
		case 0xE003:
			answer = pia->read(address - 0xE000);
			break;

		/*
			Serial port (optional)
			Used as a cassette interface on Poly 1 (with early BASIC versions)
			MC6850 ACIA (Serial Controller) at E004-E00
		*/
		case 0xE004:
		case 0xE005:
			answer = acia1->read(address - 0xE004);
			break;

		/*
			Baud rate register at 0xE006
		*/
		case 0xE006:
			answer = baud_rate_register;
			break;

		/*
			Keyboard controler
			MC6821 PIA (Parallel Controller) at E00C-E00F
		*/
		case 0xE00C:
		case 0xE00D:
		case 0xE00E:
		case 0xE00F:
			answer = pia2->read(address - 0xE00C);
			break;

		/*
			Disc Change Indicator
			bit 1 (000000x0) indicated whether the current disk drive door has been opened since the drive was last deselected.
			The bit is cleared when a new drive is selected
		*/
		case 0xE014:
			answer = fdc[drive_select & 0x03]->get_door_opened() ? 0x02 : 0x00;
			break;

		/*
			FDC
			WD1771 FDC (Floppy Drive Controller) at 0xE018-E01B
		*/
		case 0xE018:
		case 0xE019:
		case 0xE01A:
		case 0xE01B:
			answer = fdc[drive_select & 0x03]->read(address - 0xE018);
			break;

		/*
			Timer
			MC6840 Programmable Timer (Clock) at E020-E027
		*/
		case 0xE020:
		case 0xE021:
		case 0xE022:
		case 0xE023:
		case 0xE024:
		case 0xE025:
		case 0xE026:
		case 0xE027:
			answer = timer->read(address - 0xE020);
			break;

		/*
			Network
			MC6854 ADLC (network) at E030-E036
		*/
		case 0xE030:
		case 0xE031:
		case 0xE032:
		case 0xE033:
		case 0xE034:
		case 0xE035:
		case 0xE036:
			answer = network->read((address - 0xE030) / 2);
			break;

		/*
			Prot switch at 0xE040
		*/
		case 0xE040:
			answer = 0;
			break;

		/*
			DAT Dynamic Address Translation at 0xE050-E05F
		*/
		case 0xE050:
		case 0xE051:
		case 0xE052:
		case 0xE053:
		case 0xE054:
		case 0xE055:
		case 0xE056:
		case 0xE057:
		case 0xE058:
		case 0xE059:
		case 0xE05A:
		case 0xE05B:
		case 0xE05C:
		case 0xE05D:
		case 0xE05E:
		case 0xE05F:
			answer = dat[address - 0xE050];		 // remember... its in 1s compliment
			break;

		/*
			Memory Map 1 select at 0xE060
		*/
		case 0xE060:
			answer = 0;
			break;

		/*
			Memory Map 2 select at 0xE70
		*/
		case 0xE070:
			answer = 0;
			break;

		default:
			if (address < 0xE000)
				answer = memory[physical(address)];		// User mode addressing
			else if (address < 0xE800)
				answer = 0; 							// there's nothing here in the memory map
			else if (address < 0xF000)
				answer = video_memory[address - 0xE800];
			else
				answer = bios[address - 0xF000];
			break;
		}
	}

return answer;
}

/*
	POLY::WRITE()
	-------------
*/
void poly::write(word address, byte value)
{
unsigned long physical_address;

if (!prot)
	{
	/*
		User mode address translation
	*/
	physical_address = physical(address);
	if (physical_address < 0xC000 || physical_address >= 0x10000)		// else we're in the BASIC ROM
		{
		memory[physical_address] = value;
		if (physical_address >= 0x4000 && physical_address < 0x6000)
				changes |= UPDATE_DISPLAY_2 | UPDATE_DISPLAY_5;
		else if (physical_address >= 0x8000 && physical_address < 0xA000)
				changes |= UPDATE_DISPLAY_4 | UPDATE_DISPLAY_5;
		}
	}
else
	{
	/*
		Protected mode addressing
	*/
	switch (address)
		{
		/*
			Screen controller
			MC6821 PIA (Parallel Controller) at E000-E003
		*/
		case 0xE000:
		case 0xE001:
		case 0xE002:
		case 0xE003:
			changes |= UPDATE_DISPLAY_FLAGS;
			pia->write(address - 0xE000, value);
			break;

		/*
			Serial port (optional)
			Used as a cassette interface on Poly 1 (with early BASIC versions)
			MC6850 ACIA (Serial Controller) at E004-E00
		*/
		case 0xE004:
		case 0xE005:
			acia1->write(address - 0xE004, value);
			break;

		/*
			Baud rate register at 0xE006
		*/
		case 0xE006:
			baud_rate_register = value;
			break;

		/*
			Keyboard controler
			MC6821 PIA (Parallel Controller) at E00C-E00F
		*/
		case 0xE00C:
		case 0xE00D:
		case 0xE00E:
		case 0xE00F:
			pia2->write(address - 0xE00C, value);
			break;

		/*
			Drive and Side Select
			lower 2 bits are the drive; bit 6 is the side select (0x0000xx)
		*/
		case 0xE014:
			if (value != drive_select)
				{
				/*
					Only clear the status on deselect
				*/
				fdc[drive_select & 0x03]->clear_door_opened();
				drive_select = value;
				}
			break;

		/*
			FDC
			WD1771 FDC (Floppy Drive Controller) at 0xE018-E01B
		*/
		case 0xE018:
		case 0xE019:
		case 0xE01A:
		case 0xE01B:
			fdc[drive_select & 0x03]->write(address - 0xE018, value);
			break;

		/*
			Timer
			MC6840 Programmable Timer (Clock) at E020-E027
		*/
		case 0xE020:
		case 0xE021:
		case 0xE022:
		case 0xE023:
		case 0xE024:
		case 0xE025:
		case 0xE026:
		case 0xE027:
			timer->write(address - 0xE020, value);
			break;

		/*
			Network
			MC6854 ADLC (network) at E030-E036
		*/
		case 0xE030:
		case 0xE031:
		case 0xE032:
		case 0xE033:
		case 0xE034:
		case 0xE035:
		case 0xE036:
			network->write((address - 0xE030) / 2, value);
			break;

		/*
			Prot switch
			addressed at 0xE040
			In 2 clock cycles we'll leave prot.  Implemented on the Poly in hardware!
			This is done so that we can write to E040 in one instruction and then RTI in the next
			as the RTI takes 2 clock cycles.
		*/
		case 0xE040:
			cycles_before_leaving_prot = 2;
			break;

		/*
			DAT Dynamic Address Translation at 0xE050-E05F
		*/
		case 0xE050:
		case 0xE051:
		case 0xE052:
		case 0xE053:
		case 0xE054:
		case 0xE055:
		case 0xE056:
		case 0xE057:
		case 0xE058:
		case 0xE059:
		case 0xE05A:
		case 0xE05B:
		case 0xE05C:
		case 0xE05D:
		case 0xE05E:
		case 0xE05F:
			dat[address - 0xE050] = value & 0x0F;		 // this is the page lookup table (in 1s compliment)
			break;

		/*
			Memory Map 1 select at 0xE060
		*/
		case 0xE060:
			dat_bank = 0;
			break;

		/*
			Memory Map 2 select at 0xE70
		*/
		case 0xE070:
			dat_bank = 1;
			break;

		default:
			if (address < 0xE000)
				{
				/*
					User mode addressing
					This is done with a recursive call so that we can catch any writes to the screen and update the flags
				*/
				prot = FALSE;
				write(address, value);
				prot = TRUE;
				}
			else if (address < 0xE800)
				{ /* there's nothing here in the memory map */ }
			else if (address < 0xF000)
				{
				video_memory[address - 0xE800] = value;
				if (address >= 0xE800 && address < 0xEBC0)
					changes |= UPDATE_DISPLAY_1;
				else if (address >= 0xEC00 && address < 0xEFC0)
					changes |= UPDATE_DISPLAY_3;
				}
			else
				{ /* we're in the BIOS so can't write */ }
			break;
		}
	}
}

/*
	POLY::SWI()
	-----------
*/
void poly::swi(void)
{
prot = TRUE;
mc6809::swi();
}

/*
	POLY::SWI2()
	------------
*/
void poly::swi2(void)
{
prot = TRUE;
mc6809::swi2();
}

/*
	POLY::SWI3()
	------------
*/
void poly::swi3(void)
{
prot = TRUE;
mc6809::swi3();
}

/*
	POLY::DO_NMI()
	--------------
*/
void poly::do_nmi(void)
{
prot = TRUE;
mc6809::do_nmi();
}

/*
	POLY::DO_FIRQ()
	---------------
*/
void poly::do_firq(void)
{
prot = TRUE;
mc6809::do_firq();
}

/*
	POLY::DO_IRQ()
	--------------
*/
void poly::do_irq(void)
{
prot = TRUE;
mc6809::do_irq();
}

/*
	POLY::STEP()
	------------
*/
void poly::step(void)
{
long long cycles, cycles_spent, current;
long leaving_prot;

/*
	Are we counting down before leaving protected mode?
*/
cycles = this->cycles;
leaving_prot = (prot && cycles_before_leaving_prot > 0);

/*
	Do the instruction
*/
if (firqpend)
	do_firq();
else if (timer_irqpend || keyboard_irqpend || network_irqpend)		// IRQ is trigered on the state of the signal no on the rising edge.
	do_irq();

USim::step();

/*
	Send ticks to the timer
*/
cycles_spent = this->cycles - cycles;
for (current = 0; current < cycles_spent; current++)
	timer->step();

/*
	If we're counting down before leaving prot we might now leave prot
*/
if (leaving_prot && (cycles_before_leaving_prot -= cycles_spent) <= 0)
	prot = FALSE;
}

