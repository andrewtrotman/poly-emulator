# POLY Emulator for Windows (64-bit) #
Copyright (c) 2006-2010 Andrew Trotman
Released GPL

## INTRODUCTION ##
The Poly and Proteus emulator was written by Andrew Trotman
(andrew@cs.otago.ac.nz).  It is GPL because it includes other GPL code.

There are three programs included:

* network_poly.exe emulates a Poly connected via the network to a Proteus
* poly.exe         emulates a Standalone Poly without a Proteus
* proteus.exe      emulates a Proteus without a Poly

## Changes ##
* Version 1.2  Added ROM Version 3.4 and fixed 6840 and interrupt bugs (but one remains causing a hang)
* Version 1.1  Fixed shift-bug and added Epson MX-80 emulator
* Version 1.0  First working verison including Poly and Proteus
* Version 0.11 Very much a prototype

## OPERATING SYSTEMS ##
The Poly was typically connected via a network cable to a Proteus.  In
this configuration is ran POLYSYS, the Poly operating system.  However,
the development Poly had a 5.25" disk drive attached and could run FLEX.
The Proteus had a 6809 and Z80 and, with a terminal attached, could run
CP/M or FLEX.  The Progeni people who ported CP/M for the Proteus
realised that the CP/M BIOS could be addapted to run on the Poly network
and so with a Poly attached to a Proteus you can run CP/M on the Proteus
and use the Poly as a terminal.  It should be clear from this
description that the Poly / Proteus combination was not you
run-of-the-mill single-headed 8-bit machine.


### Polysys ###
You boot POLYSYS (the Poly Operating System) by using the networked Poly
emulator.  Set the ROM verison to match the version of EXTENDED
POLYBASIC on the POLYSYS boot disk.  If you do not then it will not boot
properly (not an emulator bug).  The Proteus and Poly communicate before
you log on, so set the ROM version *before* you insert a disk into the
Proteus.

### Cp/m ###
You boot Proteus CP/M using the Proteus emulator.  You boot Poly CP/M by
running the networked Poly emulator, setting the ROMs to Poly 2, and
then putting the Poly CP/M disk into the Proteus drive.

### Flex ###
You boot Proteus FLEX using the Proteus emulator.  You boot Poly FLEX by
running the Poly emulator, setting the ROMs to Dev Poly and then putting
the Poly FLEX disk into the Poly disk drive.