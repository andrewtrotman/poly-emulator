/*
	POLY_UNIT.H
	-----------
*/
#ifndef POLY_UNIT_H_
#define POLY_UNIT_H_

#ifdef _WIN32
	#include <windows.h>
#endif
#include "poly.h"
#include "proteus_unit.h"

/*
	class POLY_UNIT
	---------------
*/
class poly_unit
{
#ifdef _WIN32
	friend static LRESULT CALLBACK windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
#endif

public:
	enum { ACTIVE, PASSIVE };

private:
	static const long WIDTH_IN_PIXELS = 480;
	static const long HEIGHT_IN_PIXELS = 240;
	static const long TIMER_TEXT_FLASH = 1;
	static const long TIMER_CPU_TICK = 2;
	static const long TIMER_DISPLAY_REFRESH = 4;

private:
	#ifdef _WIN32
		HINSTANCE hInstance;
		HDC bitmap;
		HBITMAP bmp_about;
	#else
		long hInstance;
		long bitmap;
		long bmp_about;
	#endif
	poly *client;
	unsigned char *canvas;
	long bmp_about_width, bmp_about_height;
  
	unsigned char poly_key_line_insert, poly_key_char_insert, poly_key_line_del, poly_key_char_del, poly_key_calc, poly_key_help;
	unsigned char poly_key_exit, poly_key_back, poly_key_repeat, poly_key_next, poly_key_pause, poly_key_left, poly_key_right;
	unsigned char poly_key_up, poly_key_down, poly_key_u0, poly_key_u1, poly_key_u2, poly_key_u3, poly_key_u4, poly_key_u5, poly_key_u6;
	unsigned char poly_key_u7, poly_key_u8, poly_key_u9, poly_key_at, poly_key_bar, poly_key_exp, poly_key_pound, poly_key_shift_pause;
	unsigned char poly_key_keypad_dot;

	long shift;

public:
	#ifdef _WIN32
		HWND window;
	#else
		long window;
	#endif

private:
	void make_canvas(void);
	#ifdef _WIN32
		LRESULT windows_callback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
		unsigned char translate_key(WPARAM wParam);
	#else
		long windows_callback(long hwnd, long message, long wParam, long lParam);
		unsigned char translate_key(long wParam);
	#endif
	void set_keyboard_scan_codes(long poly_type);
	proteus *server;
	poly **clients;
	long text_flash_state;

protected:
	#ifdef _WIN32
		void menu(WORD clicked);
		void set_rom_version(WORD option);
		void load_disk(HWND hwnd, long menu);
	#else
		void menu(long clicked);
		void set_rom_version(long option);
		void load_disk(long hwnd, long menu);
#endif

public:
	#ifdef _WIN32
		poly_unit(HINSTANCE hInstance, proteus *server = NULL, poly **clients = NULL);
	#else
		poly_unit(long hInstance, proteus *server = NULL, poly **clients = NULL);
	#endif
	virtual ~poly_unit() {}

	#ifdef _WIN32
		INT_PTR CALLBACK about_box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
	#else
		long about_box(long hDlg, long message, long wParam, long lParam);
	#endif
	long create_window(const char *window_title, long mode = ACTIVE);
	poly *connect_to(proteus *server);
	poly *connect_to(poly *downstream);
} ;

#endif /* POLY_UNIT_H_ */
